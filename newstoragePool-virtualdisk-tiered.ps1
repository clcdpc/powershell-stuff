##
## Newer version of script to create storage pool, storage tiers, virtual disks
## This version helps figure out storage quantity for tiers

#Query what disks in the primordial pool can be part of a storage pool
$PhysicalDisks = Get-PhysicalDisk -CanPool $true

#Make sure that all disks in the pool have the same sectorsize, if they don't stop the script
#See article: http://jeffgraves.me/2014/06/03/ssds-on-storage-spaces-are-killing-your-vms-performance/
#Use New-StoragePool with -LogicalSectorSizeDefault 512
$sectortypes = ($PhysicalDisks | select PhysicalSectorSize | get-unique)
$numsectortypes = ($sectortypes | measure).Count

write-host $sectortypes
write-host $numsectortypes

if ($numsectortypes -ne 1)
{
    Write-Host "Different Sector Sizes - stopping Pool creation"
	return
}

#Create new storage pool
Get-StorageSubsystem –FriendlyName "Storage Spaces on $env:computername" | New-StoragePool –Friendlyname "ProdDB-Store" –PhysicalDisk $PhysicalDisks

#Set resiliency settings for storage pool
Get-StoragePool MainStr | Set-ResiliencySetting -Name Mirror -NumberOfColumnsDefault 2

#Create SSD Tier 
$tier_ssd = New-StorageTier -StoragePoolFriendlyName "MainStr" -FriendlyName SSD_TIER -MediaType SSD

#Create HDD Tier 
$tier_hdd = New-StorageTier -StoragePoolFriendlyName "MainStr" -FriendlyName HDD_TIER -MediaType HDD

#Get Max Tier size for SSDs (with output)
$SSDTierMaxSize = Get-StorageTierSupportedSize SSD_TIER -ResiliencySettingName Mirror | select -Expand TierSizeMax

#Get Max Tier size for HDDs (with output)
$HDDTierMaxSize = Get-StorageTierSupportedSize HDD_TIER -ResiliencySettingName Mirror | select -Expand TierSizeMax

#Create a tiered Storage Space (virtual disk) no WBCache - WORKED
Get-StoragePool "MainStr" | New-VirtualDisk -FriendlyName "MainVMStr" -ResiliencySettingName Mirror -StorageTiers $tier_ssd, $tier_hdd -StorageTierSizes $SSDTierMaxSize, $HDDTierMaxSize

#Initialize virtual disk, Create Partition and Volume, Assign Drive Letter and with type ReFS

Get-VirtualDisk MainVMStr | Get-Disk | Set-Disk -IsReadOnly 0

Get-VirtualDisk MainVMStr | Get-Disk | Set-Disk -IsOffline 0

Get-VirtualDisk MainVMStr | Get-Disk | Initialize-Disk -PartitionStyle GPT

Get-VirtualDisk MainVMStr | Get-Disk | New-Partition -DriveLetter "R" -UseMaximumSize

Initialize-Volume -DriveLetter "R" -FileSystem ReFS -Confirm:$false