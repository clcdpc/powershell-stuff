<# Author: Paul Robson

Description: This script can be used to query all running VMs in the defined VM Host, and then go through each one and enable the
VMQ on each network adapter of the VMs

#>

## cmdlet below checks the all the physical network adapters in VM Host and shows whether VMQ is enabled or not
## Get-NetAdapterVmq

## cmdlet below ENABLES the physical adapter to do VMQ. It can be disabled by changing $true to $false
## Set-NetAdapterVmq -Name "<name of NIC>" -Enabled $true

# Define VM host after - ComputerName
$VMNames = Get-VM -ComputerName "HyperV09" | ?{ $_.State -EQ "Running" } | Select VMName

foreach ($name in $VMNames)
{
	## VMQ can be disabled by changing vmqweight to 0
    Get-VMNetworkAdapter -VMName $name.VMName | Set-VMNetworkAdapter -VmqWeight 1
}