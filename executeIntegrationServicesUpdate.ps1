<# Author: Charbel Nemnom

Modified by: Paul Robson

Website: http://charbelnemnom.com

Original: https://charbelnemnom.com/2014/06/auto-update-the-installation-of-hyper-v-integration-services-with-powershell/

Description: This script can be used by PS into the Hyper-V host, and then in order to install the updates for the Integration Services
You have to enter the VM names in the variable "$VMNames" separated by commas. Ex: "ProdProbe01", "ProdPAPI", "ProdPAC"
The script might fail if there is no DVD drive assigned to the VM. Once DVD drive is added, run the script against the VM that produced
the error.

#>

#Get Credentials
$cred = Get-Credential "a.probson"

#list all of your Hyper-V Hosts
$ServerNames = "HyperV09"

$VMNames = "ProdProbe01"

foreach ($ServerName in $ServerNames)
{
	#$VMNames = Get-VM -ComputerName "HyperV09" | ?{$_.IntegrationServicesVersion -lt "6.3.9600.18398" -and $_.State -EQ "Running" } | Select VMName
	
	$VMs = $VMNames > C:\VMNames.txt
	
	If ((Get-Content "C:\VMNames.txt") -eq $Null)
	{
		Write-Output "The Integration Services Update is NOT required for Virtual Machines" " on Host" $ServerName
	}
	Else
	{
		foreach ($VirtualMachine in $VMNames)
		{
			$VMName = $VirtualMachine
			Set-VMDvdDrive -VMName $VMName -Path "C:\Windows\System32\vmguest.iso" 
			$DVDriveLetter = Get-VMDvdDrive -VMName $VMName | select -ExpandProperty id | Split-Path -Leaf
			Enable-WSManCredSSP -DelegateComputer $VMName -Role Client -Force
			Connect-WSMan $VMName -Credential $cred
			Set-Item WSMan:\$VMName*\Service\Auth\CredSSP -Value $true
			$scriptblock = {cmd.exe /C "D:\support\amd64\setup.exe /quiet"} #cmd.exe /C "D:\support\amd64\setup.exe /quiet"
			
            Invoke-Command -ComputerName $VMName -ScriptBlock $scriptblock -Authentication Credssp -Credential $cred
			
			Disconnect-WSMan $VMName
			
			Get-VMDvdDrive -VMName $VMName | Set-VMDvdDrive -Path $null
			
			" "
			" "
			Write-Output "The Integration Services has been updated for " $VMName "VM on " $ServerName "host, the VM is Rebooting..."
		}
		" "
		" "
	}
	Remove-Item -Path C:\VMNames.txt -Force
}