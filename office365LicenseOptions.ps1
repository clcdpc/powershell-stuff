<# Author: Alex Graham

Modified by: Paul Robson

Modified Date: 2/20/18

Description: This PS script can be run to enable License options or enable all license options
for Office 365 Education Plus for Faculty license

BEFORE RUNNING SCRIPT
You need to enter user email account below
#>

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -Authentication Basic -ConnectionUri https://ps.outlook.com/powershell -AllowRedirection:$true -Credential $UserCredential
Import-PSSession $Session

Connect-MsolService -Credential $UserCredential

# USER ACCOUNT NEEDED
# In command below, enter user email account 
## Enter domain that needs to have all options or select license options enabled
$Users = Get-MsolUser -All | where { $_.UserPrincipalName -notlike "*@worthingtonlibraries.org" `
                            -and $_.UserPrincipalName -notlike "*@columbuslibrary.org" `
                            -and $_.UserPrincipalName -notlike "*@bexleylibrary.org" `
                            -and $_.UserPrincipalName -notlike "*@ualibrary.org" `
                            -and $_.UserPrincipalName -ne "pplcirc@pickeringtonlibrary.org" `
                            -and $_.UserPrincipalName -ne "agraham@plaincitylib.org" `
                            -and $_.UserPrincipalName -ne "pplrefstaff@pickeringtonlibrary.org" `
                            -and $_.UserPrincipalName -ne "tsuser@pickawaylib.org" `
                            -and $_.UserPrincipalName -ne "tuser@swpl.org" `
                            -and $_.UserPrincipalName -ne "mpcyouth@plaincitylib.org" `
                            -and $_.UserPrincipalName -ne "gcrefipad@swpl.org" `
                            -and $_.UserPrincipalName -ne "cmi@fcdlibrary.org" `
                            -and $_.UserPrincipalName -notlike "scanner@*" `
                            -and $_.UserPrincipalName -notlike "scans@*" `
                            -and $_.UserPrincipalName -notlike "*@clcdpc.org" `
                            -and $_.UserPrincipalName -notlike "*@CLCohio.onmicrosoft.com" `
                            -and $_.UserPrincipalName -notlike "_CLC_Office365_Sync*" `
                            -and $_.UserPrincipalName -notlike "jobsearch@*" `
                            -and $_.UserPrincipalName -notlike "no-reply@*" `
                            -and $_.UserPrincipalName -notlike "tmp_*" `
                            -and $_.UserPrincipalName -notlike "bssi*" `
                            -and $_.UserPrincipalName -notlike "*test*" `
                            -and $_.UserPrincipalName -notlike "*staff*" `
                            -and $_.UserPrincipalName -notlike "*envision*" `
                            -and $_.UserPrincipalName -notlike "*eware*" `
                            -and $_.UserPrincipalName -notlike "*lab*" `
                            -and $_.UserPrincipalName -notlike "HealthMailbox*" } | Select-Object UserPrincipalName

$disabledPlans= @()

$disabledPlans +="OFFICESUBSCRIPTION"

##$disabledPlans +="EXCHANGE_S_STANDARD"

# Cmdlet establishes Office 365 SKU ID
$AccountSkuId = Get-MsolAccountSku | Where-Object {$_.AccountSKUID -like "*STANDARDWOFFPACK_IW_FACULTY*"}

$nooffice = New-MsolLicenseOptions -AccountSkuId $AccountSkuId.AccountSkuId -DisabledPlans $disabledPlans

$yesoffice = New-MsolLicenseOptions -AccountSkuId $AccountSkuId.AccountSkuId

$UsageLocation = "US"

$user = ""

foreach ($rmuser in $unlicensedOffice){

	Set-MsolUser -UserPrincipalName $rmuser.UserPrincipalName -UsageLocation $UsageLocation
	
	## Licenses need to be removed FIRST so then they can be added again with all license options
    #Cmdlet below used to remove a license completely
    Set-MsolUserLicense -UserPrincipalName $rmuser.UserPrincipalName -RemoveLicenses $AccountSkuId.AccountSkuId
}

foreach ($adduser in $unlicensedOffice){

	Set-MsolUser -UserPrincipalName $adduser.UserPrincipalName -UsageLocation $UsageLocation

    #Cmdlet below reissues licenses with all options
	Set-MsolUserLicense -UserPrincipalName $adduser.UserPrincipalName -AddLicenses $AccountSkuId.AccountSkuId -LicenseOptions $yesoffice
}

Remove-PSSession $Session