param (
[string]$domaincontrollers
)

$domaincontrollers = Get-VM | where {$_.Name -like 'DC*' -or $_.Name -like 'REPLICA DC*'} | ForEach-Object {$_.Name}
ForEach ($domaincontroller in $domaincontrollers) {
 $dcsname = Get-VMIntegrationService -VMName $domaincontroller | where {$_.Enabled -eq 1 -and $_.Name -eq "Time Synchronization"} | select VMName,Name,Enabled
 Write-Host $dcsname
}

$dcsname