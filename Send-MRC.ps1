# This script deletes old marc export files and then renames them and send them to collection hq

$path = "C:\ProgramData\Polaris\SRServiceRoot\TrainSR\129\scheduledjobs"
$adhocpath = "C:\ProgramData\Polaris\SRServiceRoot\TrainSR\129\adhocreports"
$scriptpath = "C:\Programdata\clc_send_CollectionHQ_data"
$hours = 25
$ext = ".mrc"
$ftpurl = "ftp.collectionhq.com"
$ftpuser = "clcoh"
$ftppass = "coy2a9rfc3"
$ftpremotepath = "/"
$dayofweektorun = "Tuesday"

# Only run this script on certain days of the week, if you remove this it will cause a PRTG sensor to trip because it will leave the scheduledjobs folder without any files
if ((get-date).DayOfWeek -ne $dayofweektorun) {
	Write-Output "Not the proper day of the week to run... exiting"
	exit   
}

# Count the number of marc files to make sure we have more than one, if none then switch to the adhoc folder
if (( Get-ChildItem -path $path | Measure-Object ).Count -eq 0) {
	Write-Output "Nothing in scheduledjobs folder, switching to adhoc folder."
	$path = $adhocpath
}

# Create new log file
$log = "$scriptpath\log\$(Get-Date -format FileDateTime).log"
if (!(test-path $log)) { New-Item -Path $log -Force | Out-Null }

# Delete all non MRC files regardless of age
Get-childitem -Path $path -File | Where-Object Extension -ne $ext | Remove-Item

# Delete all Files more than NN hours old
Get-ChildItem -path $path | Where-Object LastWriteTime -lt $($(Get-Date).AddHours(-1 * $hours)) | Remove-Item

# Enumerate the remaining file
$files = Get-ChildItem -path $path

Write-Output "Checking for files in $path" | Tee-Object -File $log -Append

# Rename files based upon THE HOUR they were created, so there will be one file NAMED for each library
# add new libraries as needed to the elseif statments, matching their simplyreports scheduled runtime
foreach ($file in $files) {

	$hourcreated = $file.CreationTime.ToString("HH")

	if ($hourcreated -eq "01") {
		$libraryname = "-clc-delaware-"
	}
	elseif ($hourcreated -eq "02") {
		$libraryname = "-clc-pickerington-"
	}
	elseif ($hourcreated -eq "03") {
		$libraryname = "-clc-worthington-"
	}
	## if the hour doesn't match any known library, then rename the file to unknown to flag CollectionHQ to get in touch with us
	else {
		$libraryname = "-clc-library-unknown-"
	}
	
	Rename-Item -Path $file.FullName -NewName "CollectionHQ$libraryname$($file.LastWriteTime.ToString('yyyyMMddTHHmmssffff'))$ext"

}

#Setup FTP session
Set-Location "C:\ProgramData\clc_send_CollectionHQ_data"

Write-Output "Setting up FTP session" | Tee-Object -File $log -Append

$uploadOptions = New-Object WinSCP.SessionOptions -Property @{
    Protocol = [WinSCP.Protocol]::Ftp
	HostName = $ftpurl
	UserName = $ftpuser
	Password = $ftppass
    FtpMode = [WinSCP.FtpMode]::Active
    };

$upload = New-Object WinSCP.Session
$upload.Open($uploadOptions)

# Enumerate the new file name(s)
$files = Get-ChildItem -path $path

#Send the files via FTP
foreach ($file in $files) {

    Write-Output "Uploading $file..." | Tee-Object -File $log -Append
    $upload.PutFiles($file.FullName, $ftpremotepath).Check()
}

## Close the FTP session
Write-Output "Closing FTP Session" | Tee-Object -File $log -Append
$upload.Dispose()