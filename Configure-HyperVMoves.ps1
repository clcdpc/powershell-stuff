﻿# Kerberos delegation to configure Live-Migration in Kerberos mode for Windows Server 2016 Hyper-V
Import-Module ActiveDirectory
# Variables
$HVHost01 = "hyperv07"
$HVHost02 = "hyperv08"
$HVHost03 = "hyperv09"
$HVHost04 = "hyperv10"
$HVHost05 = "hyperv06"

# Delegate Microsoft Virtual System Migration Service and CIFS for every other possible Live Migration host
$HV01Spns = @("Microsoft Virtual System Migration Service/$HVHost01", "cifs/$HVHost01")
$HV02Spns = @("Microsoft Virtual System Migration Service/$HVHost02", "cifs/$HVHost02")
$HV03Spns = @("Microsoft Virtual System Migration Service/$HVHost03", "cifs/$HVHost03")
$HV04Spns = @("Microsoft Virtual System Migration Service/$HVHost04", "cifs/$HVHost04")
$HV05Spns = @("Microsoft Virtual System Migration Service/$HVHost05", "cifs/$HVHost04")

$delegationProperty = "msDS-AllowedToDelegateTo"
$delegateToSpns = $HV01Spns + $HV02Spns + $HV03Spns + $HV04Spns + $HV05Spns

# Configure Kerberos to (Use any authentication protocol)
$HV01Account = Get-ADComputer $HVHost01
$HV01Account | Set-ADObject -Add @{$delegationProperty=$delegateToSpns}
Set-ADAccountControl $HV01Account -TrustedToAuthForDelegation $true

$HV02Account = Get-ADComputer $HVHost02
$HV02Account| Set-ADObject -Add @{$delegationProperty=$delegateToSpns}
Set-ADAccountControl $HV02Account -TrustedToAuthForDelegation $true

$HV03Account = Get-ADComputer $HVHost03
$HV03Account | Set-ADObject -Add @{$delegationProperty=$delegateToSpns}
Set-ADAccountControl $HV03Account -TrustedToAuthForDelegation $true

$HV04Account = Get-ADComputer $HVHost04
$HV04Account | Set-ADObject -Add @{$delegationProperty=$delegateToSpns}
Set-ADAccountControl $HV04Account -TrustedToAuthForDelegation $true

$HV05Account = Get-ADComputer $HVHost05
$HV05Account | Set-ADObject -Add @{$delegationProperty=$delegateToSpns}
Set-ADAccountControl $HV05Account -TrustedToAuthForDelegation $true

Invoke-Command -ComputerName $HVHost01 -ScriptBlock { KLIST PURGE –LI 0x3e7 }
Invoke-Command -ComputerName $HVHost02 -ScriptBlock { KLIST PURGE –LI 0x3e7 }
Invoke-Command -ComputerName $HVHost03 -ScriptBlock { KLIST PURGE –LI 0x3e7 }
Invoke-Command -ComputerName $HVHost04 -ScriptBlock { KLIST PURGE –LI 0x3e7 }
Invoke-Command -ComputerName $HVHost05 -ScriptBlock { KLIST PURGE –LI 0x3e7 }