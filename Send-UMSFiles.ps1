﻿<#
This script will upload the most recent Polaris DebtCollect files to UMS for processing.
It should be run nightly after the process that creates the files finishes running at 7am.
Instead of a password, the process uses a public/private keypair, which should be referenced after the "-KeyFile"
#>

#Pulls the latest Polaris version installed on ProdApp. Should be relatively safe way to pull the latest verion used in production. 
$latestPolaris = Get-ChildItem 'C:\Program Files (x86)\polaris' | Where-Object{ $_.PSIsContainer } | Sort-Object { $_.FullName } | Select-Object -ExpandProperty Name -Last 1

#Start log file
$folderroot = "C:\ProgramData\Polaris\$latestPolaris\Logs\SSIS\CollectionAgency\Reports"
$today = Get-Date -format yyyy-MM-dd-hh-mm-ss
$log = "$folderroot\logs\$today.txt"
New-Item -ItemType File -Path $log -Force | Out-Null

#Install SSH module
if ($null -eq (Get-Module Posh-SSH)) {
    Try   { Install-Module -Name Posh-SSH -Force }
    Catch { Write-Host "ERROR - Powershell SFTP module Post Failed To Load" }
    }
Import-Module Posh-SSH

#Credentials
$creds = New-Object System.Management.Automation.PSCredential ("clc", (new-object System.Security.SecureString)) #No password, meant for use with $key
$key = "C:\ProgramData\clc_ums_sftp\openssh.key"

#Only upload files created within the last NN hours to avoid accidentally sending duplicates
$hours = 72

#Path and IP settings
$SftpPath = "incoming"
$SftpIp = "sftp.unique-mgmt.com"

$folders = @('GHP-6','FCL-8','MPL-16','PPL-23','ALE-2','WAG-25','MPC-14','CML-39','SPL-28','WL-32','UAPL-78','BEX-84','LON-86','PCL-19') | Sort-Object #real members
#$folders = @('ABC-00') #fake member

foreach ($folder in $folders) {
$folderpath = "$folderroot\$folder"
$files = (Get-ChildItem $folderpath -file) | Select-Object -ExpandProperty FullName
write-output "Found $($files.count) files in $folder to send" | Out-File $log -Append

if (!(Test-Path $folderpath\sent)) {New-Item -Path $folderpath\sent\ -ItemType Directory -Force}

#Establish the SFTP connection
$session = (New-SFTPSession -AcceptKey -ComputerName $SftpIp -KeyFile $key -Credential $creds).SessionId #for keyfile auth

#Upload each file to the SFTP path
foreach ($file in $files) {
$lastWrite = (get-item $file).LastWriteTime
$timespan = new-timespan -hours $hours

if (((get-date) - $lastWrite) -gt $timespan) {Write-Output "$file is older than $hours hours, not transferring" | Out-File $log -Append} else {
Write-Output "Uploading $file to $sftpPath via session $session" | Out-File $log  -Append
try {Set-SFTPFile -SessionId $session -LocalFile $file -RemotePath $SftpPath} catch {write-error "Unable to send $file!" | Out-File $log -Append}
}

Move-Item -Path $file -Destination "$folderpath\sent\" -Force

}
write-output "" | Out-File $log -Append #line break between members in log file

#Disconnect SFTP session
(Get-SFTPSession -SessionId $session).Disconnect()

}