﻿#This script clones the powershell-stuff repo to the directory defined below.

$repo = "https://bitbucket.org/clcdpc/powershell-stuff/src/master/"
$local = "C:\ProgramData\clc-powershell"
$git = "\\clcdpc.org\files\CLCStaff\git"

if (test-path $local) {remove-item $local -Recurse -force}

#git refuses to run from UNC path, so a drive must be mapped
#find the first unused drive letter and map it
$alph=@()
69..90|foreach-object{$alph+=[char]$_}

foreach ($letter in $alph) {
$drive = $($letter+ ":")
if ((get-psdrive -PSProvider FileSystem).Name -like $letter) {"$drive already mapped"} else {net use $drive $git}
if ((get-psdrive -PSProvider FileSystem).DisplayRoot -like $git) {"Git mapped to $drive"; break}
}

cd $($letter+ ":")
Start-Process -FilePath "git.exe" -Argumentlist "clone -q $repo $local" -Wait
cd C:\
sleep 15

#remove mapped drive connection
net use $($letter+ ":") /delete