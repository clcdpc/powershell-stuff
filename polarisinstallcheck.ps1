$uninstallregistryhive="HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*"
$latestpolarisversionnumber="5.7"

# Check the unintall registry hive to see if this version of Polaris is listed

if (Get-ItemProperty $uninstallregistryhive `
        | Select-Object DisplayName, DisplayVersion `
        | Where-Object {$_.DisplayName -like '*Polaris Clients*' -and $_.DisplayVersion -like "*$latestpolarisversionnumber*"})
{
    Write-Output "Polaris version $latestpolarisversionnumber appears to have installed."
}
else {
    Write-Error "Did NOT find Polaris version: $latestpolarisversionnumber installed, check Polaris logs."
}

