
<#
.Synopsis
   Hi Guys Here's a powershell script i created for Automated Install of Hyper-V Integration Services in a VM 
   running on Windows Server 2012 with Hyper-V V 3.0 Role using PowerShell Remoting.

   Special Thanks to Adam with his blog http://csharpening.net/?p=1052, which gave the required inspiration to me to develop this script

   Author - Vinith Menon http://powershell-enthusiast.blogspot.in/	

.EXAMPLE

   Install-VMIntegrationService -VMName "ev.VMWIN2K8R2-1.H" -VMComputerName "VMWIN2K8R2-1" -username powershell-enth\administrator -password A2011
#>
function Install-VMIntegrationService
{
    [CmdletBinding()]
    
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        $VMName,

        # Param2 help description
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=1)]
        $VMComputerName,

          # Param2 help description
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=2)]
        $username,

          # Param2 help description
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=3)]
        $password
               
    )

    foreach ($vm in $vmname)

    {

    foreach ($comp in $VMComputerName)

    {

    $pass =  ConvertTo-SecureString  -String $password -AsPlainText -force
    $cred = New-Object System.Management.Automation.PsCredential($username,$pass)

    Set-VMDvdDrive -VMName $vm -Path "C:\Windows\System32\vmguest.iso" 

    $DVDriveLetter = Get-VMDvdDrive -VMName $vm | select -ExpandProperty id | Split-Path -Leaf

    $HostICversion= Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Virtualization\GuestInstaller\Version" | select -ExpandProperty Microsoft-Hyper-V-Guest-Installer
    
    $VMICversion = Invoke-Command -ScriptBlock {Get-ItemProperty "HKLM:\software\microsoft\virtual machine\auto" | select -ExpandProperty integrationservicesversion } -ComputerName $comp -Credential $cred

    if($HostICversion -eq $VMICversion) {

    Write-Host "Hyper-V Host IC Version and the VM $vm IC Version are the same" -Verbose

    $obj = New-Object psobject -Property @{

    'HostIntegration Services Version' =  $HostICversion
    'VMIntegration Services Version' =  $VMICversion
    'Hyper-V Host Name' = hostname
    'VirtualMachine Name'= $vm
    }

    Write-Output $obj

    Set-VMDvdDrive -VMName $vm -ControllerNumber 1 -ControllerLocation 0 -Path $null
        
    }

    else {

    $VMICversion = Invoke-Command -ScriptBlock {Get-ItemProperty "HKLM:\software\microsoft\virtual machine\auto" | select -ExpandProperty integrationservicesversion } -ComputerName $comp -Credential $cred
    Write-Host  "$vm Old Integration Services Version $VMICversion" -Verbose


    Invoke-WmiMethod -ComputerName $comp -Class Win32_Process -Name Create -ArgumentList "$($DVDriveLetter):\support\x86\setup.exe /quiet /norestart" -Credential $cred

    #$ICInstallString = "'$dvddriveletter':\support\amd64\setup.exe /quiet"
    #([WMICLASS]"\\$comp\ROOT\CIMV2:win32_process").Create($ICInstallString) | out-null 
    
    start-sleep 3          
 
    while (@(Get-Process setup -computername $comp   -ErrorAction SilentlyContinue).Count -ne 0) {
        Start-Sleep 3
        Write-Host "Waiting for Integration Service Install to Finish on $comp ..." -Verbose
    }

    Write-Host  "Completed the Installation of Integration Services" -Verbose
    Write-Host  "Restarting Computer for Changes to Take Place" -Verbose


    #Restart-Computer -ComputerName $comp -Wait -For WinRM -Force -Protocol WSMan -Credential $cred

    Restart-Computer -ComputerName $comp -Wait -For WinRM -Force -Credential $cred
    
    
    Write-Host  "$vm Is Online Now" -Verbose
    $VMICversion = Invoke-Command -ScriptBlock {Get-ItemProperty "HKLM:\software\microsoft\virtual machine\auto" | select -ExpandProperty integrationservicesversion } -ComputerName $comp -Credential $cred
    Write-Host  "$vm New Integration Services Version $VMICversion" -Verbose

    Set-VMDvdDrive -VMName $vm -ControllerNumber 1 -ControllerLocation 0 -Path $null
       

}
     



    }




    }

  



}
