﻿Function Test-Ping
{
Param($computer = "127.0.0.1")
  $ping = new-object System.Net.NetworkInformation.Ping
  Try
  {
  [void]$ping.send($computer,1)
  $Online = $true
  }
  Catch  {    $Online = $False  }
    Return $Online
}

$computers = Get-ADComputer -filter * | Select DNSHostName
$computers = $computers | Sort-Object DNSHostName
$onlinecomputers = @()

foreach ($computer in $computers) {if (Test-Ping $($computer.DNSHostName)) {$onlinecomputers += $computer}}

}
