function CreateShare($folder, $name, $object)
{
	New-SmbShare -Name $name -Path $folder -FullAccess "Everyone"
	$acl = get-acl $folder
	$acl.SetAccessRuleProtection($True, $True)
	foreach($g in $object)
	{
		Write-Host $g
		$rule = New-Object System.Security.AccessControl.FileSystemAccessRule($g, "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
		$acl.AddAccessRule($rule)
	}
	Set-Acl $folder $acl
	$acl | format-list
} 

$polarisVersion = "6.3"
$pacRoot = "c:\Program Files\Polaris\$polarisVersion\PowerPAC"

CreateShare -folder "$pacRoot\custom\themes\wl" -name "wl$polarisVersion" -object "NT_WOR\Domain Users"
CreateShare -folder "$pacRoot\custom\themes\alexandria" -name "alexandria$polarisVersion" -object "clcdpc\Domain Users"
CreateShare -folder "$pacRoot\custom\themes\southwest" -name "southwest$polarisVersion" -object "swpl\Domain Users"
CreateShare -folder "$pacRoot\custom\themes\bexley" -name "bexley$polarisVersion" -object "bexley\Domain Users"
CreateShare -folder "$pacRoot\custom\themes\uapl" -name "uapl$polarisVersion" -object "uapl\Domain Users"
