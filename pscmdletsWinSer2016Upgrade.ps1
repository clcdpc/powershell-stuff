## Mount ISO through Powershell (can use remote Powershell) with this command
Mount-DiskImage -ImagePath 'location of ISO file' -StorageType ISO -PassThru | Get-Volume

## Run setup from mounted image / disc

## Once Windows Server 2016 is installed in HyperV host, you will have to join to domain
## and rename server back to CLC naming.
## You will have to remove object with same name from AD

Rename-Computer -NewName "<enter name here>" -Restart

## Shows current computer name
$env:computername

## Add local computer to a domain using credentials and restarting

Add-Computer -ComputerName "<current computer name>" -DomainName "<domain to add>" -Credential "<credential>" -Restart

## Activate Windows via PS script

$computer = gc env:computername

$key = "XXXXX-XXXXX-XXXXX-XXXXX-XXXXX"

$service = get-wmiObject -query "select * from SoftwareLicensingService" -computername $computer

$service.InstallProductKey($key)

$service.RefreshLicenseStatus()

Install-WindowsFeature -Name "Hyper-V"

Install-WindowsFeature -Name "RDC"

Install-WindowsFeature -Name "Hyper-V-PowerShell"

Install-WindowsFeature -Name "FS-FileServer"

## ========BEGIN Network configuration for core server ============================= ##

## Query ALL VM Names and store them in variable for later use
$allvms = Get-VM | Select -ExpandProperty Name


## Disconnect ALL VMs from current Virtual Switch
foreach ($vm in $allvms) {

    Disconnect-VMNetworkAdapter -VMName $vm

}

## Connect all VMs back to a Virtual switch
foreach ($vm in $allvms) {

    Get-VMSwitch LanVirtualSwitch | Connect-VMNetworkAdapter -VMName $vm

}

Get-VMSwitch InternetAccess | Connect-VMNetworkAdapter -VMName Test1

## Create virtual LAN switch, to use 10 GB SFP+ card and set it to use VLAN 10

## THIS shows all current VM Switches in server. If you don't have any, you will have to create one. All your VMs
Get-VMSwitch
## need a switch to connect to network.

## ONLY use this cmdlet if you know for sure you want to remove switch.
# Remove-VMSwitch -Name "LanVirtualSwitch" # OR

## When changing NIC Teaming configuration, CLC has configured our default switch to use the NIC team name
## BUT if there needs to be a change to your NIC Team, you have to temporarily change the SwitchType of your default
## switch. Below, I have switched it to use a Private type. Once your changes for the NIC Team have been completed
## You can use the second command from here to set the default switch to use your NIC Team again

Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private # AND THEN switch it back to -NetAdapterName

Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName "10SFPteam"

## Cmdlet show all physical adapters and virtual adapters
Get-NetAdapter

## Cmdlet shows adapters that are up
Get-NetAdapter | where { $_.AdminStatus -eq 1 }

## THIS IS the key cmdlet that creates the NIC Teaming. We have set it up to use LACP and in your switch
## It has to be configured the same way on the switch ports (LACP or 802.3ad)
New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "NIC1","NIC2" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic

## Cmdlet that creates NEW switch and using NIC Teaming name. The last parameter allows it to be managed by
## Hyper-V host
New-VMSwitch -Name LanVirtualSwitch -NetAdapterName <use name of network adapter or NIC Teaming name> -AllowManagementOS $True

## This cmdlet sets the VM Switch to use a certain VLAN ID for the management OS (or for the Hyper-V host)
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10

## RUNS from hypervhost - sets VLAN ID for VMNetworkAdapter, for a specific VM

Set-VMNetworkAdapterVlan -VMName testws2016 -Access -VlanId 9

## ======== END of Network configuration for core server ============================= ##

Get-NetFirewallRule -DisplayName "Remote Service Management*"
nic1 / 12 ; slot 2 port 1 54

Set-NetFirewallRule -DisplayName "Remote Service Management (RPC)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Service Management (RPC-EPMAP)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Service Management (NP-In)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Event Log Management (RPC)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Event Log Management (RPC-EPMAP)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Event Log Management (NP-In)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Volume Management - Virtual Disk Service (RPC)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Volume Management (RPC-EPMAP)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Desktop - User Mode (UDP-In)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Desktop - User Mode (TCP-In)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Desktop - Shadow (TCP-In)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Windows Remote Management (WS-Management)" -Enabled True -Action Allow -Profile Domain

Set-NetFirewallRule -DisplayName "Remote Volume Management - Virtual Disk Service Loader (RPC)" -Enabled True -Action Allow -Profile Domain

Set-VMSwitch -Name LanVirtualSwitch -NetAdapterName

## Remove IP address from interface

Get-NetAdapter -interfaceindex <number> | Remove-NetIPAddress -AddressFamily IPv4 -Confirm:$false

Get-NetAdapter -interfaceindex <number> | Remove-NetRoute -AddressFamily IPv4 -Confirm:$false

New-NetIPAddress -InterfaceAlias "Ethernet 2" IPv4Address "192.168.9.11" -PrefixLength 24 -DefaultGateway 192.168.9.1

Get-NetAdapter -interfaceindex <number> | New-NetIPAddress -AddressFamily IPv4 -IPAddress <ipaddress> -PrefixLength 24 -DefaultGateway <gateway>

Get-NetAdapter -interfaceindex <number> | Set-DnsClientServerAddress -ServerAddresses "192.168.44.11","192.168.44.12"

## Add a secondary IP address

New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress "192.168.44.179" -SkipAsSource $True

Set-DnsClientServerAddress -InterfaceIndex 12 -ServerAddresses ("192.168.44.11","192.168.44.12")

## Installing and Deploying AD, DNS, GPMC
## Adding DC to existing domain

Add-WindowsFeature -Name "ad-domain-services" -IncludeAllSubFeature -IncludeManagementTools 
Add-WindowsFeature -Name "dns" -IncludeAllSubFeature -IncludeManagementTools 
Add-WindowsFeature -Name "gpmc" -IncludeAllSubFeature -IncludeManagementTools

Import-Module ADDSDeployment

Install-ADDSDomainController -DomainName <domain> -SafeModeAdministratorPassword <password>

# Create NEW Forest, add Domain Controller 
$domainname = "clctest.org" 
$netbiosName = "CLCTEST"
Import-Module ADDSDeployment
Install-ADDSForest -CreateDnsDelegation:$false -DatabasePath "C:\Windows\NTDS" -DomainMode "Win2012R2" -DomainName $domainname -DomainNetbiosName $netbiosName -ForestMode "Win2012R2" -InstallDns:$true -LogPath "C:\Windows\NTDS" -NoRebootOnCompletion:$false -SysvolPath "C:\Windows\SYSVOL" -Force:$true

## When installing ADDS in 2016 server
Install-ADDSForest -CreateDnsDelegation:$false -DatabasePath "C:\Windows\NTDS" -DomainName $domainname -DomainNetbiosName $netbiosName -InstallDns:$true -LogPath "C:\Windows\NTDS" -NoRebootOnCompletion:$false -SysvolPath "C:\Windows\SYSVOL" -Force:$true


## Create new AD user
New-ADUser -Name "Paul Robson" -GivenName Paul -Surname Robson `
-SamAccountName a.probson -UserPrincipalName a.probson@clctest.org `
-AccountPassword (Read-Host -AsSecureString "Account Password") `
-PassThru | Enable-ADAccount

## Add user to an AD group
Add-ADGroupMember "Domain Admins" a.probson

## Modify AD account password
Set-ADAccountPassword -Identity a.probson -NewPassword (Read-Host -AsSecureString "Account Password") -Reset 

## Setting up SMB folders and permissions

New-SmbShare -Name EZProxyUA -Path c:\ezproxyua -ReadAccess Everyone -FullAccess Administrator -Description "EZProxy UA"

Remove-SmbShare -Name EZProxy

Get-SmbShareAccess

Set-SmbShare -Name EZProxyUA 

Grant-SmbShareAccess -Name EZProxyUA -AccountName "CLCDPC\Domain Admins" -AccessRight Full

Grant-SmbShareAccess -Name EZProxyUA -AccountName "UAPL\IT Staff" -AccessRight Full