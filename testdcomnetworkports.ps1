 # Checks basic DCOM NETWORK communication between client and server.
 # This script should be run as a DOMAIN Administrator.
 # This does NOT check the Polaris DCOM service specifically, just DCOM Network connections.
 # If the DCOM check takes longer than 1 second it will also show as a failure.
 # Created by: Wes Osborn

 # App servers do NOT Support FQDN - so only use hostnames for testing purpoes to mimic the Polaris client
 $PolarisAppServers = "proddb","prodapp" # Update the list of app servers as needed
 $Credential = get-credential # Use domain admin account

foreach ($Computer in $PolarisAppServers) {
            try {
                $SessionOp = New-CimSessionOption -Protocol Dcom
                $CimSession = New-CimSession -ComputerName $Computer -Credential $Credential `
                    -SessionOption $SessionOp -Name $Computer -OperationTimeoutSec 1 -ErrorAction Stop
                Write-Host "DCOM Check OK for:" $Computer
                Remove-CimSession -CimSession $CimSession }
            catch {
                Write-Host "DCOM Check FAILED for:" $Computer}}