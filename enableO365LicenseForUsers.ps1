<# Author: Alex Graham

Modified by: Paul Robson

Modified Date: 1/31/17

Description: This PS script can be run to enable an Office 365 Education Plus for Faculty license
BUT disabling the Exchange Plan 1 (Email mailbox license), and Office Pro Plus license

BEFORE RUNNING SCRIPT
You need to enter user email account below
#>

<#-- ADDED on 1/31/17 variables / commands to integrate login in script --#>
## Request passwords and turns it into a secure string
#Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File "C:\users\a.probson\appdata\local\sec-string.txt"

# $azuser = "adsync@clcohio.onmicrosoft.com"
# $azpassword = Get-Content "C:\users\a.probson\appdata\local\sec-string.txt" | ConvertTo-SecureString
# $UserCredential = new-object -typename System.Management.Automation.PSCredential -argumentlist $azuser, $azpassword

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -Authentication Basic -ConnectionUri https://ps.outlook.com/powershell -AllowRedirection:$true -Credential $UserCredential
Import-PSSession $Session

Connect-MsolService -Credential $UserCredential

# USER ACCOUNT NEEDED
# In command below, enter user email account 
$Users = Get-MsolUser -All  -UnlicensedUsersOnly | where { $_.UserPrincipalName -notlike "*@worthingtonlibraries.org" -and $_.UserPrincipalName -notlike "*@columbuslibrary.org" -and $_.UserPrincipalName -notlike "*@bexleylibrary.org" -and $_.UserPrincipalName -notlike "*@ualibrary.org" -and $_.UserPrincipalName -ne "pplcirc@pickeringtonlibrary.org" -and $_.UserPrincipalName -ne "agraham@plaincitylib.org" -and $_.UserPrincipalName -ne "pplrefstaff@pickeringtonlibrary.org" -and $_.UserPrincipalName -ne "testuser@plaincitylib.org" -and $_.UserPrincipalName -notlike "*@clcdpc.org" -and $_.UserPrincipalName -notlike "*@CLCohio.onmicrosoft.com" } | Select-Object UserPrincipalName

$disabledPlans= @()

$disabledPlans +="OFFICESUBSCRIPTION"

##$disabledPlans +="EXCHANGE_S_STANDARD"

# Cmdlet establishes Office 365 SKU ID
$AccountSkuId = Get-MsolAccountSku | Where-Object {$_.AccountSKUID -like "*STANDARDWOFFPACK_IW_FACULTY*"}

$nooffice = New-MsolLicenseOptions -AccountSkuId $AccountSkuId.AccountSkuId -DisabledPlans $disabledPlans

$UsageLocation = "US"

foreach ($user in $Users){
Set-MsolUser -UserPrincipalName $user.UserPrincipalName -UsageLocation $UsageLocation
    #Cmdlet below used to remove a license completely
    #Set-MsolUserLicense -UserPrincipalName $user.UserPrincipalName -RemoveLicenses $AccountSkuId.AccountSkuId
Set-MsolUserLicense -UserPrincipalName $user.UserPrincipalName -AddLicenses $AccountSkuId.AccountSkuId
Set-MsolUserLicense -UserPrincipalName $user.UserPrincipalName -LicenseOptions $nooffice
}

#Cmdlet below checks that the Office 365 Education Plus for faculty
##Get-MsolUser -UserPrincipalName $Users.UserPrincipalName

#Cmdlet below checks for services enabled and disabled for the Office 365 Education Plus for faculty
##(Get-MsolUser -UserPrincipalName $Users.UserPrincipalName).Licenses.ServiceStatus

Remove-PSSession $Session