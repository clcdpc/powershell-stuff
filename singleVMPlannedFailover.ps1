##On the primary[1] server (“core” is the name of my VM):

$PrimaryVM1 = "<name>"
Stop-VM $PrimaryVM1
Start-VMFailover -VMName $PrimaryVM1 –prepare

## =========================================================

## Then jump to the replica server:

$ReplicaVM1 = "<name>"
Start-VMFailover -VMName $ReplicaVM1
Start-VM $ReplicaVM1

## And finally, when you also want to reverse the replication:

Set-VMReplication -reverse -VMName $ReplicaVM1