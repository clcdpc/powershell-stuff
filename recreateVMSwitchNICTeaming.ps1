## HYPERV07
========================================================================================
## Disconnects all VMs from switch
Disconnect-VMNetworkAdapter -VmName *

Remove-VMSwitch -name "LanVirtualSwitch" -confirm:$false

##Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private # AND THEN switch it back to -NetAdapterName

Get-NetAdapter | where { $_.AdminStatus -eq 1 }

Remove-NetLbfoTeam -name "10SFPTeam" -Confirm:$false

New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "SLOT 3 Port 1","SLOT 1 2 Port 1" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic -Confirm:$false

Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName "10SFPteam"

## Cmdlet that creates NEW switch and using NIC Teaming name. The last parameter allows it to be managed by
## Hyper-V host
New-VMSwitch -Name LanVirtualSwitch -NetAdapterName <use name of network adapter or NIC Teaming name> -AllowManagementOS $True

## This cmdlet sets the VM Switch to use a certain VLAN ID for the management OS (or for the Hyper-V host)
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10

Get-VM * | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "LanVirtualSwitch"

## HYPERV08
===========================================================================================================

## Disconnects all VMs from switch
Disconnect-VMNetworkAdapter -VmName *

Remove-VMSwitch -name "LanVirtualSwitch" -confirm:$false

##Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private # AND THEN switch it back to -NetAdapterName

Get-NetAdapter | where { $_.AdminStatus -eq 1 }

Remove-NetLbfoTeam -name "10SFPTeam" -Confirm:$false

New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "SLOT 1 4 Port 1","SLOT 1 3 Port 1" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic -Confirm:$false

Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName "10SFPteam"

## Cmdlet that creates NEW switch and using NIC Teaming name. The last parameter allows it to be managed by
## Hyper-V host
New-VMSwitch -Name LanVirtualSwitch -NetAdapterName <use name of network adapter or NIC Teaming name> -AllowManagementOS $True

## This cmdlet sets the VM Switch to use a certain VLAN ID for the management OS (or for the Hyper-V host)
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10

Get-VM * | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "LanVirtualSwitch"

## HYPERV09
===========================================================================================================

## Disconnects all VMs from switch
Disconnect-VMNetworkAdapter -VmName *

Remove-VMSwitch -name "LanVirtualSwitch" -confirm:$false

##Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private # AND THEN switch it back to -NetAdapterName

Get-NetAdapter | where { $_.AdminStatus -eq 1 }

Remove-NetLbfoTeam -name "10SFPTeam" -Confirm:$false

New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "SLOT 2 Port 1","SLOT 4 Port 1" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic -Confirm:$false

Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName "10SFPteam"

## Cmdlet that creates NEW switch and using NIC Teaming name. The last parameter allows it to be managed by
## Hyper-V host
New-VMSwitch -Name LanVirtualSwitch -NetAdapterName <use name of network adapter or NIC Teaming name> -AllowManagementOS $True

## This cmdlet sets the VM Switch to use a certain VLAN ID for the management OS (or for the Hyper-V host)
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10

Get-VM * | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "LanVirtualSwitch"

## HYPERV06
===========================================================================================================

## Disconnects all VMs from switch
Disconnect-VMNetworkAdapter -VmName *

Remove-VMSwitch -name "LanVirtualSwitch" -confirm:$false

##Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private # AND THEN switch it back to -NetAdapterName

Get-NetAdapter | where { $_.AdminStatus -eq 1 }

Remove-NetLbfoTeam -name "10SFPTeam" -Confirm:$false

New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "SLOT 2 Port 1","NIC1" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic -Confirm:$false

Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName "10SFPteam"

## Cmdlet that creates NEW switch and using NIC Teaming name. The last parameter allows it to be managed by
## Hyper-V host
New-VMSwitch -Name LanVirtualSwitch -NetAdapterName <use name of network adapter or NIC Teaming name> -AllowManagementOS $True

## This cmdlet sets the VM Switch to use a certain VLAN ID for the management OS (or for the Hyper-V host)
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10
## The VLAN ID used above should be TAGGED in the physical switch ports this plugs into. 
## Also for certain HP switches the PVID on the switch ports should NOT be the VLAN ID used above, it should be in the accepted list, but not the primary/untagged VLAN for the port.

Get-VM * | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "LanVirtualSwitch"

## HYPERV10
===========================================================================================================

## Fresh start
Disconnect-VMNetworkAdapter -VmName *
Remove-VMSwitch -name "LanVirtualSwitch" -confirm:$false
Remove-NetLbfoTeam -name "10SFPTeam" -Confirm:$false

#Creates Team with one NIC on on each card
New-NetLbfoTeam -Name "10SFPteam" -TeamMembers "SLOT 7 Port 1","NIC1" -TeamingMode Lacp -LoadBalancingAlgorithm Dynamic -Confirm:$false

#Creates and configures Virtual Switch with correct settings
New-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName 10SFPTeam -AllowManagementOS $True
Set-VMSwitch -Name "LanVirtualSwitch" -SwitchType Private
Set-VMSwitch -Name "LanVirtualSwitch" -NetAdapterName 10SFPTeam

#Configures Hyper-V host to use said adapter, and use it on VLAN 10
Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10
## The VLAN ID used above should be TAGGED in the physical switch ports this plugs into. 
## Also for certain HP switches the PVID on the switch ports should NOT be the VLAN ID used above, it should be in the accepted list, but not the primary/untagged VLAN for the port.

#Configures IP and DNS settings for Hyper-V host
New-NetIPAddress –IPAddress 192.168.10.111 -DefaultGateway 192.168.10.1 -PrefixLength 24 -InterfaceIndex 17
Set-DNSClientServerAddress –InterfaceIndex 17 –ServerAddresses 192.168.10.15,192.168.10.16

#Configures any existing VMs to use new switch
Get-VM * | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "LanVirtualSwitch"

