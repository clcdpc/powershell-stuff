<#
Author: Micahel Otey

Description: This simple script queries system user is logged into and shows OS info

#>

# Get Operating System Info
$sServer = $env:COMPUTERNAME
$sOS =Get-WmiObject -class Win32_OperatingSystem -computername $sServer

foreach($sProperty in $sOS)
{
   Write-Host $sServer
   write-host $sProperty.Description
   write-host $sProperty.Caption
   write-host $sProperty.OSArchitecture
   write-host $sProperty.ServicePackMajorVersion
}