<# Author: Paul Robson

Description: Retrieves EDI files from Recorded books FTP site

	Using: https://gallery.technet.microsoft.com/scriptcenter/PowerShell-FTP-Client-db6fe0cb

#>

## Import credentials from file, and put into variables
$Data = Import-csv -Path "C:\Users\probson\Documents\powershell-main\powershell-stuff\FTPEDIInvoices\ftpcreds.csv"

## Imports FTP modules downloaded from website in above comment
Import-Module PSFTP

foreach ($ftpsite in $data) {

$ftpserver = $ftpsite.server
$ftpuser = $ftpsite.username
$ftppassword = $ftpsite.password
$ftpseccreds = ConvertTo-SecureString -String $ftppassword -Force -AsPlainText
$ftpcreds = New-Object System.Management.Automation.PSCredential ($ftpuser,$ftpseccreds)
$ftppath = $ftpsite.path
$ftpfilter = $ftpsite.filter
$ftplocal = $ftpsite.pcpath

Write-Host $ftpserver

## Sets FTP connection with FTP creds provided from CSV
Set-FTPConnection -Credentials $ftpcreds -Server "ftp://$ftpserver" -Session clcTestSession -UsePassive
## Creates FTP connection
$ftpSession = Get-FTPConnection -Session clcTestSession

## Downloads all files from FTP site, into the $localpath
Get-FTPChildItem -Session $ftpSession -Path $ftppath -Filter "*$ftpfilter" | Get-FTPItem -Session $ftpSession -LocalPath $ftplocal

## Removes files from FTP site
Get-FTPChildItem -Session $ftpSession -Path /outbound -Filter * | Remove-FTPItem -Session $ftpSession

}