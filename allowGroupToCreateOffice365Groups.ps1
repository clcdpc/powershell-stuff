<# Author: Paul Robson

Description: This script can be used to allow a group to create Office 365 groups.
Anyone that is part of this groups. Global administrators can still create Groups

Resources:
https://support.office.com/en-us/article/Manage-who-can-create-Office-365-Groups-4c46c8cb-17d0-44b5-9776-005fced8e618

#>

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -Authentication Basic -ConnectionUri https://ps.outlook.com/powershell -AllowRedirection:$true -Credential $UserCredential
Import-PSSession $Session

Import-Module AzureADPreview

Connect-AzureAD -Credential $UserCredential

Connect-MsolService -Credential $UserCredential

Get-AzureADGroup -SearchString "AllowedCreateO365Group"

$Template = Get-AzureADDirectorySettingTemplate | where {$_.DisplayName -eq 'Group.Unified'}

$Setting = $Template.CreateDirectorySetting()

New-AzureADDirectorySetting -DirectorySetting $Setting

$Setting = Get-AzureADDirectorySetting -Id (Get-AzureADDirectorySetting | where -Property DisplayName -Value "Group.Unified" -EQ).id

$Setting["EnableGroupCreation"] = $False

$Setting["GroupCreationAllowedGroupId"] = (Get-AzureADGroup -SearchString "AllowedCreateO365Group").objectid

Set-AzureADDirectorySetting -Id (Get-AzureADDirectorySetting | where -Property DisplayName -Value "Group.Unified" -EQ).id -DirectorySetting $Setting

(Get-AzureADDirectorySetting).Values

## Removes restriction on who can create groups

#$SettingId = Get-AzureADDirectorySetting -All $True | where-object {$_.DisplayName -eq "Group.Unified"}
#Remove-AzureADDirectorySetting –Id $SettingId.Id

## Query OWA Mailbox Policy for Office 365 Group creation
#get-OwaMailboxPolicy

## Set OWA mailbox policy to allow Office 365 Group creation
#set-OwaMailboxPolicy -GroupCreationEnabled $true -Identity OwaMailboxPolicy-Default

Remove-PSSession $Session