<# Author: Léon Bouquiet

Modified by: Paul Robson

Source Site: https://blogs.infosupport.com/managing-ssrs-reports-with-powershell/

Created on Feb 28, 2017

Description: Script needs to be run in the SSRS Report servers. The script looks
at all reports under the path /Polaris and then downloads them the path in variable
$twd

Second part is to upload RDLs back to server. It has all been commented out.

#>

$url = "https://prodreports.clcohio.org/ReportServer/ReportService2010.asmx"
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
$ssrs = New-WebServiceProxy -Uri $url -UseDefaultCredential -Namespace SSRS

# List everything(!) on the Report Server, recursively
#$catalogItems = $ssrs.ListChildren("/", $true)
#$catalogItems

$twd = "C:\Temp\rdls-pre73"

if (test-path $twd) {write-output "$twd directory present"} else {write-output "creating directory"; New-Item -ItemType Directory -Force -Path $twd}

# Download all Reports from a specific folder to .rdl files in the current 
# directory.
$sourceFolderPath = "/Polaris/Custom"
$items = $ssrs.ListChildren($sourceFolderPath, $true)

$items | Where-Object { $_.TypeName -eq "Report" } | Foreach-Object {
    $filename = ("{0}.rdl" -f $_.Name)

    Write-Output ("Downloading ""{0}""..." -f $_.Path)
    $bytes = $ssrs.GetItemDefinition($_.Path)
    [System.IO.File]::WriteAllBytes("$twd\$filename", $bytes)
}

Compress-Archive -Path $twd -DestinationPath $twd\RDLBackup$(get-date -f yyyy-MM-dd)
Remove-Item $twd\*.rdl

# Upload all .rdl files in the current directory to a specific folder, and 
# set their datasource references to the same shared datasource (should 
# already be deployed).
<# $targetFolderPath = "/Reports/MyNewReports"
$targetDatasourceRef = "/Data Sources/mySharedDataSource"
$warnings = $null

Get-ChildItem *.rdl | Foreach-Object {
    $reportName = [System.IO.Path]::GetFileNameWithoutExtension($_.Name)
    $bytes = [System.IO.File]::ReadAllBytes($_.FullName)

    Write-Output "Uploading report ""$reportName"" to ""$targetFolderPath""..."
    $report = $rs.CreateCatalogItem(
        "Report",         # Catalog item type
        $reportName,      # Report name
        $targetFolderPath,# Destination folder
        $true,            # Overwrite report if it exists?
        $bytes,           # .rdl file contents
        $null,            # Properties to set.
        [ref]$warnings)   # Warnings that occured while uploading.

    $warnings | ForEach-Object {
        Write-Output ("Warning: {0}" -f $_.Message)
    }
#>
    # Get the (first) *design-time* name of the data sources that the 
    # uploaded report references. Note that this might be different from 
    # the name of the datasource as it is deployed on the report server!
    
	##$referencedDataSourceName = (@($rs.GetItemReferences($report.Path, "DataSource")))[0].Name

    # Change the datasource for the report to $targetDatasourceRef
    # Note that we can access the types such as DataSource with the prefix 
    # "SSRS" only because we specified that as our namespace when we 
    # created the proxy with New-WebServiceProxy.
<#  $dataSource = New-Object SSRS.DataSource
    $dataSource.Name = $referencedDataSourceName      # Name as used when designing the Report
    $dataSource.Item = New-Object SSRS.DataSourceReference
    $dataSource.Item.Reference = $targetDatasourceRef # Path to the shared data source as it is deployed here.
    $ssrs.SetItemDataSources($report.Path, [SSRS.DataSource[]]$dataSource)
#>