#& .\Prepare-PolarisAppServer.ps1

#SMTP
Add-WindowsFeature SMTP-Server,Web-Mgmt-Console,WEB-WMI | Out-Null #Adds the feature if it is not already present 
$smtpInstance = Get-WmiObject -Namespace root\MicrosoftIISv2 -Query "Select * From IisSmtpServerSetting" #Create object with current settings
$smtpInstance.RelayIpList = @( 24, 0, 0, 128, 32, 0, 0, 128, 140, 0, 0, 128, 148, 0, 0, 128, 1, 0, 0, 0, 156, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 4, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 72, 1, 0, 128, 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 240, 0, 0, 0, 80, 1, 0, 128, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 88, 1, 0, 128, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 254, 0, 0, 0, 96, 1, 0, 128, 2, 0, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 156, 0, 0, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 24, 0, 0, 128, 32, 0, 0, 128, 60, 0, 0, 128, 68, 0, 0, 128, 1, 0, 0, 0, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 76, 0, 0, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 10, 0, 0, 0, 24, 0, 0, 128, 32, 0, 0, 128, 60, 0, 0, 128, 68, 0, 0, 128, 1, 0, 0, 0, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 76, 0, 0, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 192, 168, 0, 0, 255, 0, 0, 0, 10, 0, 0, 0, 255, 240, 0, 0, 172, 16, 0, 0, 255, 255, 0, 0, 192, 168, 0, 0, 255, 255, 254, 0, 66, 213, 44, 0  ) #Add IP addresses to allow relay
$smtpInstance.Put() | Out-Null #Save

$smtpSite = [ADSI]"IIS://Localhost/smtpsvc/1" #Create object with current settings
$smtpSite.Put("LogType", 1) #Enable logging
$smtpSite.Put("LogExtFileFlags", 4194303) #Enable logging for all events
$smtpSite.Put("ServerState", 2) #Start site
$smtpSite.SetInfo() #Save

Set-Service -Name SMTPSVC -StartupType Automatic #Configure service to start automatically at boot