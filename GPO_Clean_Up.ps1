#Author Jamal McClendon

#Delete Current GPO "Windows Defender"and push new policies out the Polaris Machines.

$Computerlist = get-content \\dc45-dsc\c$\temp\CLC_Polaris_Servers.txt

Foreach ($computer in $computerlist)
{
Get-ChildItem –Path \\$computer\c$\Windows\System32\GroupPolicy\ | Remove-Item -Recurse –Force
Restart-Computer -ComputerName $computer -force
}