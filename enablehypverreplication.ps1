﻿#This script must be run from the HyperV host that contains the VMs that need replication enabled
#Depending on network conditions, you may not be able to enable replication on all VMs at once, watch the net

#comma delimited list of guest vms to enable replication for
$ListOfReplicaServers = ("ProdApp","ProdApp01", "ProdApp02")

#server were replicas will be stored
$ReplicaServer = "hyperv07"

#dispaly current replication configuration
Get-VMReplication

foreach ($Server in $ListOfReplicaServers)
{
#Remove any exisiting replication and setup a new replication and start the intial replication process
Remove-VMReplication -VMName $Server
Enable-VMReplication $Server -ReplicaServerName $ReplicaServer -ReplicaServerPort 80 -AuthenticationType Kerberos -CompressionEnabled 1
Start-VMInitialReplication $Server
Start-Sleep -s 15
}

#display replication configuration after the script has run
Get-VMReplication