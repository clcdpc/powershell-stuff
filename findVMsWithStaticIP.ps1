<# Author: Paul Robson

Description: use this script to search in each HyperV host for VMs with static IP addresses.

#>

$hyperv-vms = Invoke-Command -ComputerName hyperv06 -ScriptBlock { Get-VM | Select -ExpandProperty VMName }
$filteredvms = @()

## Searches for VMs using specific IPv4 subnet, AND that has DHCP disabled or using static IP address
foreach ($vm in $hyperv-vms) {

$filteredvms += Invoke-Command -ComputerName $vm -ScriptBlock { Get-WmiObject -Class Win32_NetworkAdapterConfiguration | `
    where {$_.IPEnabled -eq $true -and $_.IPAddress -like "192.168.144.*" -and $_.DHCPEnabled -eq $false }  } | Select -ExpandProperty PSComputername `
    -ErrorAction Ignore

}

## Sets IPv4 DNS server addresses on network adapters with name starting with Ethernet, using filtered VMs from query above
## CHANGE DNS SERVER ADDRESSES ACCORDING TO SUBNET
foreach ($vm in $filteredvms) {

Invoke-Command -ComputerName $vm -ScriptBlock { Get-NetAdapter | where {$_.InterfaceAlias -like 'Ethernet*' } | `
	Set-DnsClientServerAddress -ServerAddresses "192.168.144.12","192.168.144.15" }

}

## Gets DNS server addresses on network adapters with name starting with Ethernet, using filtered VMs from query above
foreach ($vm in $filteredvms) {

Invoke-Command -ComputerName $vm -ScriptBlock { Get-NetIPAddress | where {$_.InterfaceAlias -like 'Ethernet*' -and $_.AddressFamily -eq "IPv4" } | `
    Get-DnsClientServerAddress }

}