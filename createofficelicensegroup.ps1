# Creates a group of users to be used for office 365 license purposes, also populates the group with users who have an email address in their AD record
# The group name is unique per library to make locating within azure ad easier
# Note, you must also set the usagelocation via aad connect to assign a license
# Finally you must also add the group to via Azure AD: https://portal.azure.com/#blade/Microsoft_AAD_IAM/ProductOverviewMenuBlade/Groups/accountSkuId/CLCohio%3ASTANDARDWOFFPACK_IW_FACULTY/skuId/78e66a63-337a-4a9a-8959-41c6654dfb56

$domainname = Get-ADDomain -Current LocalComputer

$groupname = "All" + $domainname.NetBIOSName + "Office365Users"

New-ADGroup -Name $groupname -SamAccountName $groupname -GroupCategory Security -GroupScope DomainLocal -Path "CN=Users,$domainname" -Description "Members of this group will have a CLC Office 365 tenant licese applied"

$Destination_Group = "cn=$groupname,CN=Users,$domainname"

$Target = Get-ADUser -Filter {EmailAddress -like '*' -and Enabled -eq $true}
foreach ($Person in $Target) { 
    Add-ADGroupMember -Identity $Destination_Group -Members $Person.distinguishedname 
}

$numusers = $Target.Count

Write-Output "$groupname has been created with $numusers users"