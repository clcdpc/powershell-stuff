<# Author: Paul Robson

Description: This script will find all computers and servers in $workingdomain. The script will then query what programs
were installed in October 2017, as well as the specified program names (Polaris Pre-requisites).

ROUGH DRAFT - ROUGH DRAFT - Works in CLC Domain

#>

$group1 = Get-ADGroup "Domain Computers"

## Change to domain you want to query
$basedomain = "bpl"

## Change to domain you are working on
$workingdomain = Get-ADTrust -Filter * | where { $_.DistinguishedName -like "*$basedomain*" } | Select-Object -ExpandProperty Target

## CHANGE Server parameter to desired domain to search, and also the DistinguishedName

$testdcpcs = Get-ADComputer -Server $workingdomain -Filter * | where { $_.ObjectClass -like 'computer' } | Select-Object -ExpandProperty DNSHostname

## Change variable to list a single workstation if you want to query only one computer
# $testdcpcs = "fcdl-15.fcddpc

## This cmdlet creates a file in the Path location to store the information queried in the loop below
New-Item -ItemType File -Name $workingdomain-programs.txt -Path C:\CLC-CDT\

## This loop queries through registry key and looks for Polaris requisites, and Polaris app, as well as anything installed
## in the date specified (ex. October 2017)
foreach ($dcpc in $testdcpcs)
{
    Write-Output $dcpc | Out-File -FilePath C:\CLC-CDT\$workingdomain-programs.txt -Append

    Invoke-Command -cn $dcpc -ScriptBlock {Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* `
        | select DisplayName, Publisher, InstallDate `
        | Where {$_.DisplayName -like 'Microsoft .NET Framework 4*' `
        -or $_.DisplayName -like 'Microsoft SQL Server 2008*' `
        -or $_.DisplayName -like 'Microsoft SQL Server 2012*' `
        -or $_.DisplayName -like 'Microsoft Visual C++ 2015*' `
        -or $_.DisplayName -like 'Microsoft Visual C++ 2012*' `
        -or $_.DisplayName -like 'Microsoft Visual C++ 2010*' `
        -or $_.DisplayName -like 'Microsoft SQL Server Compact*' `
        -or $_.DisplayName -like 'MSXML*' `
        -or $_.DisplayName -like 'Polaris Client*' `
        -or $_.InstallDate -like '201710*' } `
        | Format-Table -AutoSize } -WarningAction Ignore | Out-File -FilePath C:\CLC-CDT\$workingdomain-programs.txt -Append

}
## Commands to search for specific program and uninstall it
#$app1 = Get-WmiObject Win32_Product -ComputerName "bpl0-sla04.bpl.local" | where { $_.Name -like "Microsoft SQL Server 2008*" }
#$app1
#$app1.Uninstall()