<# Author: Paul Robson

Description: This script will find the groups that contain PCs as members. Then the PCs or servers in this group
will query the name of the PCs / servers only.
The Test-Path cmdlet will check in those computers if the 5.x directory has been created

ROUGH DRAFT - ROUGH DRAFT - Works in CLC Domain

#>

$group1 = Get-ADGroup "Domain Computers"

$basedomain = "bpl"

## Change to domain you are working on
$workingdomain = Get-ADTrust -Filter * | where { $_.DistinguishedName -like "*$basedomain*" } | Select-Object -ExpandProperty Target

## CHANGE Server parameter to desired domain to search, and also the DistinguishedName

$testdcpcs = Get-ADComputer -Server $workingdomain -Filter * | where { $_.ObjectClass -like 'computer' } | Select-Object -ExpandProperty DNSHostname

New-Item -ItemType File -Name $workingdomain-cdt.txt -Path C:\CLC-CDT\

foreach ($dcpc in $testdcpcs)
{
	## Change name of text file if running for a different library
    Write-Output $dcpc | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
	## Check Polaris 5.2
    Write-Output $env:ProgramFiles"\Polaris\5.2" | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Test-Path -Path "\\$dcpc\C$\Program Files\Polaris\5.2" -ErrorAction SilentlyContinue | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Write-Output ${env:ProgramFiles(x86)}"\Polaris\5.2" | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Test-Path -Path "\\$dcpc\C$\Program Files(x86)\Polaris\5.2" -ErrorAction SilentlyContinue | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
	## Check Polaris 5.6
    Write-Output $env:ProgramFiles"\Polaris\5.6" | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Test-Path -Path "\\$dcpc\C$\Program Files\Polaris\5.6" -ErrorAction SilentlyContinue | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Write-Output ${env:ProgramFiles(x86)}"\Polaris\5.6" | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
    Test-Path -Path "\\$dcpc\C$\Program Files(x86)\Polaris\5.6" -ErrorAction SilentlyContinue | Out-File -FilePath C:\CLC-CDT\$workingdomain-cdt.txt -Append
	## Checks the directory in detail
	##Get-ChildItem -Path "\\$dcpc\C$\Program Files\Polaris\5.6" -Recurse
}