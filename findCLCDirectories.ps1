<# Author: Paul Robson

Description: This script can be used to query all workstations/servers in AD, and looks for any directory
in C:\ and C:\ProgramData\ that starts with clc_
The results are stored in file findCLC_Delete.txt

#>

$alldomainpcs = Get-ADComputer -Filter * | where { $_.Name -notlike "DC*" -and $_.Name -notlike "Alex*" } | select Name

Start-Transcript "C:\temp\findCLC_Delete.txt" -Append

foreach($workstation in $alldomainpcs)
{
    $workstation = $workstation.Name
    Write-Host $workstation
    Get-ChildItem -Path "\\$workstation\c$\" -Filter clc_* -Recurse -Directory -ErrorAction SilentlyContinue
    Get-ChildItem -Path "\\$workstation\c$\ProgramData" -Filter clc_* -Recurse -Directory -ErrorAction SilentlyContinue
}

Stop-Transcript