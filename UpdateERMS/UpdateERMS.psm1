<#
.SYNOPSIS
Replaces ERMS executable

.DESCRIPTION
The script looks for servers that start with Dev*, Train*, or Prod* . Then looks into those servers and narrows down to list only the
ones running the ERMS service.
After that, in each of those servers multiple services are stopped because ERMS depends on them. The script checks status of those
services.
The old ERMSDaemon.exe is removed, and the new one is copied from the patched one in DEVDB.
Then all the services are restarted.

.PARAMETER polEnvironment
The CLC environment that you would like to target. Dev = development, Train = training, Prod = Production.
The query will get the servers in all these environmnets. If you use * it will query all servers in OU=CLC Polaris Servers, DC=clcdpc, DC=org

.PARAMETER ERMSSvcName
Enter the name of the ERMS service. Currently it is ERMSDaemon.5.6

.PARAMETER ERMSPatch
Location of the ERMS patched executable. Currently it is \\devdb\Build5.6.125.0\POL-5717\ERMSDaemon.exe

.PARAMETER ERMSProviderDLL
Location of the ERMS patched executable. Currently it is \\devdb\Build5.6.125.0\POL-5717\ERMSProvider.dll

.PARAMETER polarisVersion
Enter the version of Polaris. It currently is 5.6

#>
Function Update-ERMS {
	[CmdletBinding()]
    Param(
        [String]
        $polEnvironment = "Dev",
		
		[String]
        $ERMSSvcName = "ERMSDaemon.5.6",

        [String]
        $ERMSPatch = "\\devdb\Build5.6.125.0\POL-5717\ERMSDaemon.exe",
		
		[String]
        $ERMSProviderDLL = "\\devdb\Build5.6.125.0\POL-5717\ERMSProvider.dll",
		
		[String]
        $polarisVersion = "5.6"

        )

	#$ERMSService = "ERMSDaemon.5.6"
	#$ERMSPatch = "\\devdb\Build5.6.125.0\17DecPolarisERMS\ERMSDaemon.exe"
	$LocalERMS = "C:\Program Files\Polaris\$polarisVersion\Bin\ERMSDaemon.exe"
	
	$LocalERMSDLL = "C:\Program Files\Polaris\$polarisVersion\Bin\ERMSProvider.dll"

	$whereiserms = Get-ADComputer -Filter * -SearchBase 'OU=CLC Polaris Servers, DC=clcdpc, DC=org' `
		| where {$_.DNSHostname -like "$polEnvironment*" } `
		| select -ExpandProperty DNSHostName

	# Remotely get servers that are running the ERMS service

	$serverswitherms = Invoke-Command -ComputerName $whereiserms -ScriptBlock { Get-Service -Name $Using:ERMSSvcName -ErrorAction SilentlyContinue } `
			| Select -ExpandProperty PSComputerName

	foreach ($srvr in $serverswitherms) {

		Write-Verbose "`n$srvr `r========================"
		
		## Command stops ERMS and all other services that it depends on
		Invoke-Command -ComputerName $srvr -ScriptBlock { Stop-Service SQLSERVERAGENT,MSSQLSERVER, `
			INNReachConnector,WinHttpAutoProxySvc,netprofm,NlaSvc,Dhcp,EventLog,$Using:ERMSSvcName `
			-Force -ErrorAction SilentlyContinue -Verbose }

		## Command checks status of those services previously stopped
		Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Service -Name SQLSERVERAGENT,MSSQLSERVER, `
            INNReachConnector,WinHttpAutoProxySvc,netprofm,NlaSvc,Dhcp,EventLog,$Using:ERMSSvcName `
			-ErrorAction SilentlyContinue }

		## Old ERMSDaemon.exe is removed
		# Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Item -Path $Using:LocalERMS | Remove-Item -Verbose }
		
		## Old ERMSDaemon.exe is renamed
		Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Item -Path $Using:LocalERMS | `
			Rename-Item -NewName "$Using:LocalERMS.$(Get-Date -Format 'yyyyMMddHHmmssfff')" -Force -Verbose }
			
		## Old ERMSProvider.dll is renamed
		Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Item -Path $Using:LocalERMSDLL | `
			Rename-Item -NewName "$Using:LocalERMSDLL.$(Get-Date -Format 'yyyyMMddHHmmssfff')" -Force -Verbose }

		## ERMS patch is copied from DEVDB to local Polaris 5.6 directory
		Copy-Item -Path $ERMSPatch -Destination "\\$srvr\c$\Program Files\Polaris\$polarisVersion\Bin" -Verbose
		
		## ERMSDLL patch is copied from DEVDB to local Polaris 5.6 directory
		Copy-Item -Path $ERMSProviderDLL -Destination "\\$srvr\c$\Program Files\Polaris\$polarisVersion\Bin" -Verbose

		## Command restarts new all services previously stopped
		Invoke-Command -ComputerName $srvr -ScriptBlock { Start-Service SQLSERVERAGENT,MSSQLSERVER, `
			INNReachConnector,WinHttpAutoProxySvc,netprofm,NlaSvc,Dhcp,EventLog,$Using:ERMSSvcName `
			-ErrorAction SilentlyContinue -Verbose }
	}
}
