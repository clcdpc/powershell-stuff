<# Author: Paul Robson

Description: Enable Bitlocker via PowerShell

#>

Install-WindowsFeature -Name Bitlocker -IncludeManagementTools -IncludeAllSubFeature

## IMPORTANT: Before running this command, in the BIOS of server under Security you have to turn on TPM without Pre-Boot...
## and Activate it. Otherwise the command below will not work

## Cmdlet encrypts C drive
Enable-BitLocker -MountPoint "C:" -EncryptionMethod Aes256 –UsedSpaceOnly –RecoveryPasswordProtector

## Cmdlet encrypts R drive
Enable-BitLocker -MountPoint "R:" -EncryptionMethod Aes256 –UsedSpaceOnly –RecoveryPasswordProtector


## Cmdlet lists all drives and shows which ones are encrypted and which ones are not
Get-BitLockerVolume