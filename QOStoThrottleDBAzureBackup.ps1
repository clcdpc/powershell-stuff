<# Authors: Wes Osborn & Paul Robson

Description: The script will create a QoS Policy to throttle down bandwidth usage via service that runs the SQL Server 2014 Backup to Azure.
		The script will create this QoS Policy at 08:00 hours, and then remove it at 20:00 hours, every day.

#>

$lt = Get-WmiObject -Class Win32_LocalTime

Get-Date -Hour $lt.Hour -Minute $lt.Minute > $null

[int]$hour = $lt.Hour

$qosName = "AzureBackup"

while ( ($hour -ge 8 -and $hour -le 19) ) {

	$checkQoSPolicy = Get-NetQosPolicy -PolicyStore ActiveStore | where {$_.Name -eq $qosName} | select Name
	
	if ( $checkQoSPolicy.Name -ne $qosName) {
		
		New-NetQosPolicy -Name $qosName -IPPortMatchCondition 443 -ThrottleRateActionBitsPerSecond 200MB -PolicyStore ActiveStore > $null
		
	}
	else {
	
		break
	}
}

if ( $hour -eq 20 ) {
	
	Remove-NetQosPolicy -name $qosName -PolicyStore ActiveStore -Confirm:$false
}

	#Checks if QoS policy is in place
#Get-NetQosPolicy -PolicyStore Activestore