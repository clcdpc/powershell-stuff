#It is NOT unusual for this script to take several minutes to run

#You MUST change these settings
$shared_mailbox_primarysmtp = "mailbox@shared.org" #Enter email address for SHARED mailbox
$mailbox_user = "user@domain.org" #Enter email address for USER to added to the shared mailbox

# Use the credentials for the adsync account in lastpass
$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
Import-PSSession $Session

Add-MailboxPermission -Identity $shared_mailbox_primarysmtp -AccessRights FullAccess -InheritanceType All -AutoMapping:$true -User $mailbox_user
Add-RecipientPermission -Identity $shared_mailbox_primarysmtp -AccessRights SendAs -Confirm:$false -Trustee $mailbox_user

# Check the results
Get-MailboxPermission -Identity $shared_mailbox_primarysmtp | Where-Object {($_.IsInherited -eq $false) -and -not ($_.User -like "NT AUTHORITY\SELF")}
Start-Sleep 2
Get-RecipientPermission -Identity $shared_mailbox_primarysmtp | Where-Object {($_.IsInherited -eq $false) -and -not ($_.Trustee -like "NT AUTHORITY\SELF")}