#This process fixes error 6313 for Adsync, it should be run on AADConnect servers that are throwing this application error
Stop-Service ADSync
Remove-Item -Path HKLM:\SYSTEM\CurrentControlSet\Services\ADSync\Performance
New-Item -Path HKLM:\SYSTEM\CurrentControlSet\Services\ADSync -Name Performance -Force
unlodctr.exe ADSync
lodctr.exe "C:\Program Files\Microsoft Azure AD Sync\Bin\mmsperf.ini"
Start-Service ADSync