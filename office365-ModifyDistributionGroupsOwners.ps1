<# Author: Paul Robson

Description: use this script to modify owners of Distribution groups

#>

## Connect to Office 365
$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
Import-PSSession $Session

Connect-MsolService -Credential $UserCredential

## Query names of Distro groups and users that manage it
$managedby = Get-DistributionGroup -Identity "#SWPL*" | select displayname,managedby

$NewManagedBy = @()

## This loop goes through the users managing distro group and removes user specified
## The users left managing the distro groups are put into the NewManagedBy variable
ForEach($per in $ManagedBy)
{
	## Removes specific user from managedby
    $per.ManagedBy.Remove("UserFirstName UserLastName")

	## users left managing distro groups
    $NewManagedBy += $per
}

## This loop actually SETS / changes users managing Distro groups
foreach ($group in $NewManagedBy) {

    Set-DistributionGroup $group.displayname -ManagedBy $group.managedby -BypassSecurityGroupManagerCheck

}
