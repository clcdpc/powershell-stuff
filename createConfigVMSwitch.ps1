## PS Script to create New VM Switch

New-VMSwitch -Name LanVirtualSwitch -NetAdapterName NIC1 -AllowManagementOS $True

Get-VMNetworkAdapter -SwitchName LanVirtualSwitch -ManagementOS | Set-VMNetworkAdapterVlan -Access -VlanId 10