<# Author: Paul Robson

Description: This script can be used to copy the full content of an mailbox to a folder in another mailbox.
This can be used to migrate emails or copy emails from one person leaving to another.

http://www.jijitechnologies.com/blogs/copy-move-emails-between-office365-mailboxes-using-powershell

#>

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -Authentication Basic -ConnectionUri https://ps.outlook.com/powershell -AllowRedirection:$true -Credential $UserCredential
Import-PSSession $Session

Connect-MsolService -Credential $UserCredential

## ADsync account has to be part of the following two roles in order to perform Search-Mailbox cmdlet
## Add-RoleGroupMember -Identity "Discovery Management" -Member adsync@clcohio.onmicrosoft.com
## New-RoleGroup -Name "ExchOn Import-Export" -Roles "Mailbox Import Export" -Members adsync@clcohio.onmicrosoft.com

$sourceEmail = "probson@clcohio.org"
$TargetUserName="Paul Test"
$TargetFolderName="OldPaul"

Get-Mailbox $sourceEmail -ResultSize Unlimited | Search-Mailbox -TargetMailbox $TargetUserName -TargetFolder "$TargetFolderName" -LogLevel Full -SearchDumpster:$false

Remove-PSSession $Session