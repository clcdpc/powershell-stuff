﻿Function Clone-RvVm
{
    <#
    .NOTES
        Version: 1.0

    .SYNOPSIS
        Create a copies of Hyper-V virtual machines (VM) or their snapshots. Such copying is usually referred as cloning. You do not have to have VMM (System Center Virtual Machine Manager) for the cloning that by default is not included in the Hyper-V Manager.

    .DESCRIPTION
        Developer
            Developer: Rudolf Vesely, http://rudolfvesely.com/
            Copyright (c) Rudolf Vesely. All rights reserved
            License: Free for private use only

            RV are initials of the developer's name Rudolf Vesely and distingue names of Rudolf Vesely's cmdlets from the other cmdlets. Type “Get-Command *-Rv*” (without quotes) to list all Rudolf Vesely’s cmdlets (functions).

        Description
            Create a copies of Hyper-V virtual machines (VM) or their snapshots. Such copying is usually referred as cloning. You do not have to have VMM (System Center Virtual Machine Manager) for the cloning that by default is not included in the Hyper-V Manager.

            1500 lines of code only for cloning. This cmdlet (function) can be designated as copying on steroids. There is a large number of configurations and possibilities how the cmdlet can make copies of the VMs.

            Possibilities
                It is possible to copy whole VM in the current state with all snapshots (checkpoints).

                It is possible to copy defined snapshots and create a new VM without snapshots (checkpoints).

                It is possible to clone one object (VM or snapshot) or multiple objects at the same time by piping objects to this cmdlet or by passing arguments into the parameters of this cmdlet.

            Functions and hardcoded functionalities
                The cmdlet (function) have all behaviors as the default cmdlets so you can pipe or pass any object and the cmdlet will take care of it. But the cmdlet is also created for easy usage with no typing so you can just run the cmdlet with no parameters and the cmdlet (Windows PowerShell console) will show you a list of all VMs on the current host and the list of all snapshots and you just have to pick the item and watch the cloning process.

                The cloning process is made to be very secure.
                    It is not possible to create VMs with the same name. Of course it is not problem to have VMs with the same name but with the different ID but it is confusing to have multiple VMs with the same name so the cmdlet will create a unique names or you can manually specify the new names.

                    It is not possible to overwrite any existing file or add file into directory between other files (so you do not have to research what file belongs to what VM).

                When the cloning is done a very useful tasks are made. For example it is possible to specify to disconnect all NICs, to optimize VHDs and to set static MACs to dynamic.

                Copied virtual machines (clones) are always located in the single directories.
                    Structure
                        <VM name>/Snapshots
                            All snapshots
                        <VM name>/Virtual Hard Disks
                            HDDs from the one VM
                        <VM name>/Virtual Machines
                            Configuration file of a single VM.

                    Note from the developer: I prefer when all items of the single VM is located in one directory. You are free to move particular items when the clone is created. For example you can move system, database or application disks to a Cluster Shared Volumes (CSV) with Tier 1 disks and the data disk (file share) to Tier 2 CSV.

        Advices
            Use -Verbose switch. All Rudolf Vesely's cmdlets (functions) are verbose with a lot of details.

        Requirements
            Developed and tested on PowerShell 4.0 but should be functional on PowerShell 3.0.

        Installation
            Put text ". C:\Path\Where\The\PS1\File\Is\Saved\this_file.ps1" (without quotes) into one of the PowerShell profile files (for example in the Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1).

    .PARAMETER VmSource
        Virtual machine objects (Microsoft.HyperV.PowerShell.VirtualMachine)

        Specifies the input to this cmdlet. You can use this parameter, or you can pipe the input to this cmdlet.

    .PARAMETER VmSourceName
        Name of the source virtual machine that is the VM that should be cloned.

        When the VM object or the VM name is not provided a list all VMs on the host is displayed and user can pick the right VM.

    .PARAMETER VmSourceSnapshot
        VM snapshot (checkpoint) objects (Microsoft.HyperV.PowerShell.VMSnapshot)

        Specifies the input to this cmdlet. You can use this parameter, or you can pipe the input to this cmdlet.

    .PARAMETER VmSourceSnapshotName
        Name of the snapshot of the source virtual machine that is the VM snapshot that should be cloned.

        When the "Snapshot" item type is chosen and VM snapshot object or snapshot name is not provided a list of snapshots of the defined VM or VMs is displayed and user can pick the right snapshot.

    .PARAMETER VmTargetName
        Name of the target virtual machine (name of the newly created virtual machine that is clone of the source VM). If the name is not provided it is used source name with defined suffix.

        Naming convention:
            <Source VM name> - VM - Clone - <Date and time>
            <Source VM name> - <Snapshot name> - Snapshot - Clone - <Date and time>

    .PARAMETER ItemType
        Specification of the object that is cloned.

        There are two possibilities:
            VM
                Whole virtual machine in the current state with all snapshots (checkpoints)
            Snapshot
                One of the snapshots of the VM

    .PARAMETER VmTargetDirectoryName
        Name of the new directory in the virtual machine storage that is created for the created clone. When the name is not provided a directory is named as the name of target virtual machine without special characters.

    .PARAMETER PathDirectoryVmStorage
        Path to directory where all virtual machines are saved in it's own directories.

        Examples: D:\Virtual, C:\ClusterStorage\Volume1, \\smb30server\\VMs

        It is handy to hardcode default value of this path so the parameter do not have to be provided

    .PARAMETER PathDirectoryTemporaryData
        Path to the directory with temporary data where the source virtual machine is exported. After success cloning the exported data are removed. It is recommended to use volume on local hard drive but is it also possible to use fast file share or Cluster Shared Volume.

        Examples: D:\Temp

        It is handy to hardcode default value of this path so the parameter do not have to be provided

    .PARAMETER ComputerName
        Name of the Hyper-V host. By default local Hyper-V server is used.

    .PARAMETER AskForVmTargetName
        When the name of the new VM or VMs is not provided a user is asked to type the name or the multiple names for all VMs.

    .PARAMETER GenerateUniqueVmTargetName
        Generate unique names of the clones when the VM with the desired name already exists. Unique names are created using unique identifier.

        Naming convention:
            <Same name>
                Similar to "<Same name> - 0" but the zero is not included.
            <Same name> - 1
            <Same name> - 2
            ...

        Handy when multiple VMs or snapshots should be cloned and the name of the new clones is defined. In this case without this switch only first object is cloned and the other objects are skipped due error.

    .PARAMETER RemoveTemporaryData
        If the operations are success at the end the exported VM is removed without asking.

    .PARAMETER Configuration
        List of the configuration actions that should be performed at the end.
            Values
                Defined
                    OptimizeDisk
                        Optimize VHD and VHDx.
                    DisconnectNetwork
                        Disconnect all connected NICs
                    StaticMacToDynamic
                        On all NICs set all static MAC addresses to dynamic

    .PARAMETER Force
        Prompts for confirmation before running the cmdlet are disabled (same as -Confirm:$false).

    .EXAMPLE
        # Show dialog to choose one VM and then clone it
        Clone-RvVm -Verbose

        # or:
        Clone-RvVm -AskForVmTargetName -Verbose

    .EXAMPLE
        # Show dialog to choose one VM and one snapshot and then clone it
        Clone-RvVm -ItemType Snapshot -Verbose

        # or:
        Clone-RvVm -ItemType Snapshot -AskForVmTargetName -Verbose

        # or: It is possible to use "Clone-RvVmSnapshot" and not "Clone-RvVm -ItemType Snapshot"

    .EXAMPLE
        # Show dialog to choose two snapshots and then clone it
        Clone-RvVm -VmSourceName VmToClone0, VmToClone1 -ItemType Snapshot -Verbose

        # or:
        Get-VM -Name VmToClone0, VmToClone1 | Clone-RvVm -ItemType Snapshot -Verbose

        # or: It is possible to use "Clone-RvVmSnapshot" and not "Clone-RvVm -ItemType Snapshot"

    .EXAMPLE
        # Clone two VMs
        Clone-RvVm -VmSourceName VmToClone0, VmToClone1 -Verbose

        # or:
        Clone-RvVm -VmSourceName VmToClone0, VmToClone1 -VmTargetName 'NewVmClone' -GenerateUniqueVmTargetName -Verbose

        # or:
        Get-VM -Name VmToClone0, VmToClone1 | Clone-RvVm -Verbose

        # or:
        Get-VM -Name VmToClone0, VmToClone1 | Clone-RvVm -VmTargetName 'NewVmClone' -GenerateUniqueVmTargetName -Verbose

        # or: It is possible to use "Clone-RvVmSnapshot" and not "Clone-RvVm -ItemType Snapshot"

    .EXAMPLE
        # Clone same snapshot on two VMs (the snapshot have same name)
        Clone-RvVm -ItemType Snapshot -VmSourceName VmToClone0, VmToClone1 -VmSourceSnapshotName 'My First Snapshot' -Verbose

        # or:
        Clone-RvVm -ItemType Snapshot -VmSourceName VmToClone0, VmToClone1 -VmSourceSnapshotName 'My First Snapshot' -VmTargetName 'NewSnapshotClone' -GenerateUniqueVmTargetName -Verbose

        # or:
        Get-VM -Name VmToClone0, VmToClone1 | Clone-RvVm -ItemType Snapshot -VmSourceSnapshotName 'My First Snapshot' -Verbose

        # or:
        Get-VM -Name VmToClone0, VmToClone1 | Clone-RvVm -ItemType Snapshot -VmSourceSnapshotName 'My First Snapshot' -VmTargetName 'NewSnapshotClone' -GenerateUniqueVmTargetName -Verbose

        # or: It is possible to use "Clone-RvVmSnapshot" and not "Clone-RvVm -ItemType Snapshot"

    .EXAMPLE
        # Clone directly passed snapshots
        Clone-RvVm -VmSourceSnapshot (Get-VMSnapshot -VMName VmToClone0, VmToClone1 | Select-Object -First 2) -Verbose

        # or:
        Clone-RvVm -VmSourceSnapshot (Get-VMSnapshot -VMName VmToClone0, VmToClone1 | Select-Object -First 2) -VmTargetName 'NewSnapshotClone' -GenerateUniqueVmTargetName -Verbose

        # or:
        Get-VMSnapshot -VMName VmToClone0, VmToClone1 | Select-Object -First 2 | Clone-RvVm -Verbose

        # or:
        Get-VMSnapshot -VMName VmToClone0, VmToClone1 | Select-Object -First 2 | Clone-RvVm -VmTargetName 'NewSnapshotClone' -GenerateUniqueVmTargetName -Verbose

        # or: It is possible to use "Clone-RvVmSnapshot" and not "Clone-RvVm -ItemType Snapshot"

    .INPUTS
        Microsoft.HyperV.PowerShell.VirtualMachine
        Microsoft.HyperV.PowerShell.VMSnapshot

    .OUTPUTS
        Microsoft.HyperV.PowerShell.VirtualMachine

    .LINK
        http://rudolfvesely.com/
    #>

    [CmdletBinding(
        DefaultParametersetName = 'VmSource',
        SupportsShouldProcess = $true,
        ConfirmImpact = 'Medium'
    )]

    Param
    (
        [Parameter(
            Mandatory = $false,
            Position = 0,
            ParameterSetName = 'VmSource',
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [Microsoft.HyperV.PowerShell.VirtualMachine[]]
        $VmSource,

        [Parameter(
            Mandatory = $false,
            Position = 0,
            ParameterSetName = 'VmSourceName',
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(0,255)]
        [string[]]
        $VmSourceName,

        [Parameter(
            Mandatory = $false,
            Position = 0,
            ParameterSetName = 'VmSourceSnapshot',
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [Microsoft.HyperV.PowerShell.VMSnapshot[]]
        $VmSourceSnapshot,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(0,255)]
        [string]
        $VmSourceSnapshotName,

        [Parameter(
            Mandatory = $false,
            Position = 1,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(0,255)]
        [string]
        $VmTargetName,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateSet(
            'VM',
            'Snapshot'
        )]
        [string]
        $ItemType = 'VM',

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(0,255)]
        [string]
        $VmTargetDirectoryName,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(1,255)]
        [string]
        $PathDirectoryVmStorage = 'C:\VMs',

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateLength(1,255)]
        [string]
        $PathDirectoryTemporaryData = 'C:\Temp',

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [Alias('ComputerNames')]
        [ValidateLength(1,255)]
        [string]
        $ComputerName = '.',

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [switch]
        $AskForVmTargetName = $false,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [switch]
        $GenerateUniqueVmTargetName = $true,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [switch]
        $RemoveTemporaryData = $true,

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [ValidateSet(
            'OptimizeDisk',
            'DisconnectNetwork',
            'StaticMacToDynamic'
        )]
        [string[]]
        $Configuration = @('OptimizeDisk', 'DisconnectNetwork', 'StaticMacToDynamic'),

        [Parameter(
            Mandatory = $false,
            # Position = ,
            # ParameterSetName = ,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            ValueFromRemainingArguments = $false
            # HelpMessage = ''
        )]
        [switch]
        $Force = $false
    )

    Begin
    {
        Import-Module Hyper-V -ErrorAction SilentlyContinue -Debug:$false -Verbose:$false
        if (!(Get-Module Hyper-V -ErrorAction SilentlyContinue)) { Write-Error 'Hyper-V module is missing.' }

        if ($PSBoundParameters.Debug)
        {
            Clear-Host
        }



        <#
        Functions
        #>

        Function New-RvDirectoryOrThrow
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [string]
                $Path
            )

            if (!(Test-Path -Path $Path))
            {
                try
                {
                    $null = New-Item -Path $Path -ItemType directory -ErrorAction Stop
                }
                catch
                {
                    Write-Error ('It is not possible to create directory: {0}' -f $Path)
                    throw 'Directory have to exist and have to be accessible.'
                }
            }
        }

        Function Remove-RvFileDirectoryIncorrectCharacters
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [string]
                $InputObject
            )

            $output = $InputObject -Replace ':', ''
            $output = ($output -Replace '[^a-zA-Z0-9._-]', ' ').Trim()
            $output = $output -replace '\s+', ' ' # Multiple spaces

            # Return
            $output
        }

        Function Get-RvCountOfObjectsInTheText
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                $InputObject
            )

            if ($InputObject.Count -gt 1) { ' ({0})' -f [string]$InputObject.Count } else { '' }
        }

        Function Get-RvDateTime
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            # Return
            (Get-Date).ToUniversalTime()
        }

        Function Get-RvDateTimeForOutput
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [Parameter(
                    Mandatory = $false,
                    # Position = ,
                    # ParameterSetName = ,
                    ValueFromPipeline = $false,
                    ValueFromPipelineByPropertyName = $false,
                    ValueFromRemainingArguments = $false
                    # HelpMessage = ''
                )]
                [AllowNull()]
                [System.DateTime]
                $DateTime,

                [Parameter(
                    Mandatory = $false,
                    # Position = ,
                    # ParameterSetName = ,
                    ValueFromPipeline = $false,
                    ValueFromPipelineByPropertyName = $false,
                    ValueFromRemainingArguments = $false
                    # HelpMessage = ''
                )]
                [string]
                $Format = 'yyyy-MM-dd HH:mm:ssZ'
            )

            if (!$DateTime) { $DateTime = (Get-Date).ToUniversalTime() }

            # Return
            Get-Date -Date $DateTime -Format $Format
        }

        Function Get-RvDateTimeDuration
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [System.DateTime]
                $Start,
                [System.DateTime]
                $End
            )

            # Return
            $End - $Start
        }

        Function Get-RvDateTimeDurationForOutput
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [System.DateTime]
                $Start,
                [System.DateTime]
                $End
            )

            # Return
            (Get-RvDateTimeDuration -Start $Start -End $End) -replace '^(.*\..).*$', '$1'
        }

        Function Remove-RvItemFromList
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only

                Description
                    One item array or list is automatically converted in PowerShell to the item (no array or list).
            #>

            Param
            (
                $InputObject,
                [int]$RemoveAt
            )

            if ($InputObject.Count -lt 2)
            {
                $null
            }
            else
            {
                $InputObject.RemoveAt($RemoveAt)
            }
        }

        Function Get-RvComputerName
        {
            <#
            .DESCRIPTION
                Developer
                    Developer: Rudolf Vesely, http://rudolfvesely.com/
                    Copyright (c) Rudolf Vesely. All rights reserved
                    License: Free for private use only
            #>

            Param
            (
                [Parameter(
                    Mandatory = $true,
                    Position = 0,
                    # ParameterSetName = ,
                    ValueFromPipeline = $false,
                    ValueFromPipelineByPropertyName = $false,
                    ValueFromRemainingArguments = $false
                    # HelpMessage = ''
                )]
                [string[]]
                $ComputerName,

                [Parameter(
                    Mandatory = $false,
                    Position = 1,
                    # ParameterSetName = ,
                    ValueFromPipeline = $false,
                    ValueFromPipelineByPropertyName = $false,
                    ValueFromRemainingArguments = $false
                    # HelpMessage = ''
                )]
                [ValidateSet(
                    'ComputerName',
                    'DotOrComputerName',
                    'DescriptionAndComputerName'
                )]
                [string]
                $Output = 'ComputerName'
            )

            foreach ($computerNameItem in $ComputerName)
            {
                if ($Output -eq 'ComputerName')
                {
                    if ($computerNameItem -eq '.' -or $computerNameItem -eq 'localhost')
                    {
                        $env:COMPUTERNAME
                    }
                    else
                    {
                        $computerNameItem
                    }
                }
                elseif ($Output -eq 'DotOrComputerName')
                {
                    if ($computerNameItem -eq 'localhost' -or $computerNameItem -eq $env:COMPUTERNAME)
                    {
                        '.'
                    }
                    else
                    {
                        $computerNameItem
                    }
                }
                else # $Output -eq 'DescriptionAndComputerName'
                {
                    if ($computerNameItem -eq '.' -or $computerNameItem -eq 'localhost' -or $computerNameItem -eq $env:COMPUTERNAME)
                    {
                        'Local computer: {0}' -f $env:COMPUTERNAME
                    }
                    else
                    {
                        $computerNameItem
                    }
                }
            }
        }



        <#
        Initialization
        #>

        Write-Verbose '- Set-RvNetIpAddress: Start'
        Write-Verbose '    - Copyright (c) Rudolf Vesely (http://rudolfvesely.com/). All rights reserved'
        Write-Verbose '        - License: Free for private use only'

        $processesAllStartDateTimeUtc = Get-RvDateTime
        Write-Verbose '    - Processes: All'
        Write-Verbose ('        - Start: {0}' -f (Get-RvDateTimeForOutput -DateTime $processesAllStartDateTimeUtc))



        <#
        Paths
        #>

        # Paths
        New-RvDirectoryOrThrow -Path $PathDirectoryVmStorage
        New-RvDirectoryOrThrow -Path $PathDirectoryTemporaryData
    }

    Process
    {
        <#
        Obtaining items
        #>

        $vmSourceItems = New-Object System.Collections.ArrayList # List is needed to use RemoveAt
        $vmSourceSnapshotItems = New-Object System.Collections.ArrayList # List is needed to use RemoveAt
        Write-Verbose ''
        Write-Verbose ('    - Obtaining VMs and snapshots:')


        <#
        Obtaining items from the objects
        #>

        # Snapshots
        if ($VmSourceSnapshot)
        {
            $ItemType = 'Snapshot'

            foreach ($vmSourceSnapshotItem in $VmSourceSnapshot)
            {
                $null = $vmSourceItems.Add($(Get-VM -Id $vmSourceSnapshotItem.VMId -ComputerName $ComputerName))
                $null = $vmSourceSnapshotItems.Add($vmSourceSnapshotItem)
            }
            Write-Verbose ('        - Obtained snapshots{0}: {1}' -f $(Get-RvCountOfObjectsInTheText -InputObject $vmSourceItems), ($vmSourceSnapshotItems.Name -join ', '))
            Write-Verbose ('            - Extracted VMs from the snapshots (only for example){0}: {1}' -f $(Get-RvCountOfObjectsInTheText -InputObject $vmSourceItems), ($vmSourceItems.Name -join ', '))
        }

        # VMs
        elseif ($VmSource)
        {
            $vmSourceItems = [System.Collections.ArrayList]$VmSource
            Write-Verbose ('        - Obtained VM{0}: {1}' -f $(Get-RvCountOfObjectsInTheText -InputObject $vmSourceItems), ($vmSourceItems.Name -join ', '))
        }



        <#
        Obtaining items from the names
        #>

        # VMs
        if ($VmSourceName)
        {
            :vmSourceNameItem foreach ($vmSourceNameItem in $VmSourceName)
            {
                Write-Verbose ('        - Searching VMs from the obtained VM name: {0}' -f $VmSourceNameItem)

                try
                {
                    $vm = Get-VM -Name $vmSourceNameItem -ErrorAction Stop

                    if ($vm.Count -lt 1)
                    {
                        Write-Verbose ('            - Cannot obtain')
                        Write-Error ('Virtual machine "{0}" cannot be obtained.' -f $VmSourceNameItem)
                        continue vmSourceNameItem
                    }
                    elseif ($vm.Count -gt 1)
                    {
                        Write-Verbose ('            - Multiple servers with this name. None of them will be cloned.')
                        Write-Error ('There are multiple virtual machines with the name "{0}". The whole VM will be skipped.' -f $VmSourceNameItem)
                        continue vmSourceNameItem
                    }

                    Write-Verbose ('            - State: {0}' -f $vm.State)

                    if ($vm.State -like '*Critical*')
                    {
                        Write-Verbose ('            - VM is in the critical state and cannot be cloned.')
                        Write-Error ('Virtual machine "{0}" is in the critical state and cannot be cloned.' -f $VmSourceNameItem)
                        continue vmSourceNameItem
                    }

                    $null = $vmSourceItems.Add($vm)
                    Write-Verbose ('            - Obtained')
                }
                catch
                {
                    Write-Verbose ('            - Cannot obtain')
                    Write-Error ('Virtual machine "{0}" cannot be obtained.' -f $VmSourceNameItem)
                }
            }
        }

        # Snapshots
        if ($vmSourceItems -and $VmSourceSnapshotName)
        {
            $i = 0
            # $vmSourceItemsModified = $vmSourceItems
            $vmSourceItemsModified = New-Object System.Collections.ArrayList
            $vmSourceItems | Foreach-Object { $vmSourceItemsModified += $_ } # Copying like $vmSourceItemsModified = $vmSourceItems (this just put same reference into new variable)
            :vmSourceNameItem foreach ($vmSourceItem in $vmSourceItems)
            {
                Write-Verbose ('        - Searching for snapshot "{0}" on the already obtained VM: {1}' -f $VmSourceSnapshotName, $vmSourceItem.Name)

                try
                {
                    $vmSnapshot = Get-VMSnapshot -VM $vmSourceItem -Name $VmSourceSnapshotName -ErrorAction Stop

                    if ($vmSnapshot.Count -lt 1)
                    {
                        Write-Verbose ('            - Cannot obtain')
                        Write-Error ('Snapshot "{0}" cannot be obtained.' -f $VmSourceSnapshotName)
                        $vmSourceItemsModified = Remove-RvItemFromList -InputObject $vmSourceItemsModified -RemoveAt $i
                        continue vmSourceNameItem
                    }
                    elseif ($vmSnapshot.Count -gt 1)
                    {
                        Write-Verbose ('            - Multiple snapshots with this name. None of them will be cloned.')
                        Write-Error ('There are multiple snapshosts with the name "{0}". The whole VM will be skipped.' -f $VmSourceSnapshotName)
                        $vmSourceItemsModified = Remove-RvItemFromList -InputObject $vmSourceItemsModified -RemoveAt $i
                        continue vmSourceNameItem
                    }

                    $vmSourceSnapshotItems += $vmSnapshot
                    Write-Verbose ('            - Obtained')
                }
                catch
                {
                    Write-Verbose ('            - Cannot obtain')
                    Write-Error ('Snapshot "{0}" cannot be obtained. The whole VM will be skipped.' -f $VmSourceSnapshotName)
                    $vmSourceItemsModified = Remove-RvItemFromList -InputObject $vmSourceItemsModified -RemoveAt $i
                    continue vmSourceNameItem
                }

                ++$i
            }

            $vmSourceItems = $vmSourceItemsModified
        }



        <#
        Tests
        #>

        if (($VmSourceSnapshot -or $VmSourceSnapshotName) -and
            !$vmSourceSnapshotItems)
        {
            Write-Error 'It was not possible to obtain snapshots according passed arguments.'
            return
        }

        if (($VmSource -or $VmSourceName -or $VmSourceSnapshot) -and
            !$vmSourceItems)
        {
            Write-Error 'It was not possible to obtain VMs according passed arguments.'
            return
        }



        <#
        Dialog to choose the item (items was not provided)
        #>

        # VMs
        if (!$vmSourceItems)
        {
            $vms = Get-VM -ComputerName $ComputerName -ErrorAction SilentlyContinue

            if (!$vms)
            {
                throw 'Error: There are no VMs in the host...'
            }

            $vms = $vms | Sort-Object `
                -Property `
                    @{Expression = 'Name'; Descending = $false}, `
                    @{Expression = 'State'; Descending = $false}

            $vmCounter = 0
            $vmCriticalStateCounter = 0
            $vmsList = @()
            foreach ($vm in $vms)
            {
                if ($vm.State -match '.*Critical.*')
                {
                    ++$vmCriticalStateCounter
                }
                else
                {
                    $vmsList += [PsCustomObject]@{'Number' = $vmCounter; Name = $vm.Name; State = $vm.State; CreationTime = $vm.CreationTime; VM = $vm}

                    ++$vmCounter
                }
            }

            if (!$vmsList)
            {
                Write-Error 'Error: There are no VMs in non-critical state... The whole VM will be skipped.'
                continue vmSourceNameItem
            }

            do
            {
                # List of all items
                ''
                '--------------------------------------------------'
                ''
                'Name of the VM was not provided...'
                ''
                ''
                'All VMs on the "{0}" host:' -f (Get-RvComputerName -ComputerName $ComputerName -Output ComputerName)
                '--------------------------------------------------'
                ''
                $vmsList | Format-Table -Property Number, Name, State, CreationTime -AutoSize
                if ($vmCriticalStateCounter) { 'VMs in critical state: {0}' -f $vmCriticalStateCounter }
                ''
                '--------------------------------------------------'
                ''

                # Capturing input
                $vmListChoosedItem = Read-Host 'Please choose the number of the VM'

                $vm = $null
                foreach ($vmsListItem in $vmsList)
                {
                    if ([string]$vmsListItem.Number -eq [string]$vmListChoosedItem) # Not possible to convert to int.
                    {
                        $vm = $vmsListItem.VM
                        break
                    }
                }

                # Testing
                if (!$vm)
                {
                    'Error: No snapshot selected...'
                    '#########################################################################'
                }
                else
                {
                    $null = $vmSourceItems.Add($vm)
                }
            }
            while (!$vm)
        }

        # Snapshots
        if ($ItemType -eq 'Snapshot' -and !$vmSourceSnapshotItems)
        {
            foreach ($vmSourceItem in $vmSourceItems)
            {
                $vmSnapshots = Get-VMSnapshot -VM $vmSourceItem -ErrorAction SilentlyContinue

                if (!$vmSnapshots)
                {
                    Write-Error ('Error: There are no snapshots on the "{0}" VM. The whole VM will be skipped.' -f $vmSourceItem.Name)
                    continue vmSourceNameItem
                }

                $vmSnapshots = $vmSnapshots | Sort-Object `
                    -Property `
                        @{Expression = 'CreationTime'; Descending = $false}, `
                        @{Expression = 'Name'; Descending = $false}

                $vmSnapshotCounter = 0
                $vmDeletedStateCounter = 0
                $vmSnapshotsList = @()
                foreach ($vmSnapshot in $vmSnapshots)
                {
                    if ($vmSnapshot.IsDeleted -eq $true)
                    {
                        ++$vmDeletedStateCounter
                    }
                    else
                    {
                        $vmSnapshotsList += [PsCustomObject]@{'Number' = $vmSnapshotCounter; Name = $vmSnapshot.Name; SnapshotType = $vmSnapshot.SnapshotType; CreationTime = $vmSnapshot.CreationTime; ParentSnapshotName = $vmSnapshot.ParentSnapshotName; Snapshot = $vmSnapshot}

                        ++$vmSnapshotCounter
                    }
                }

                if (!$vmSnapshotsList)
                {
                    Write-Error 'Error: There are no snapshosts in non-deleted state... The whole VM will be skipped.'
                    continue vmSourceNameItem
                }

                do
                {
                    # List of all items
                    ''
                    '--------------------------------------------------'
                    ''
                    'Name of the snapshot was not provided...'
                    ''
                    ''
                    'All snapshots of the "{0}" VM on the "{1}" host:' -f $vmSourceItem.Name, (Get-RvComputerName -ComputerName $ComputerName -Output ComputerName)
                    '--------------------------------------------------'
                    ''
                    $vmSnapshotsList | Format-Table -Property Number, Name, SnapshotType, CreationTime, ParentSnapshotName -AutoSize
                    if ($vmDeletedStateCounter) { 'Snapshots in deleted state that are still in the configuration file: {0}' -f $vmDeletedStateCounter }
                    ''
                    '--------------------------------------------------'
                    ''

                    # Capturing input
                    $vmSnapshotsListChoosedItem = Read-Host 'Please choose the number of the snapshot'

                    $vmSnapshot = $null
                    foreach ( $vmSnapshotsListItem in  $vmSnapshotsList)
                    {
                        if ([string]$vmSnapshotsListItem.Number -eq [string] $vmSnapshotsListChoosedItem) # Not possible to convert to int.
                        {
                            $vmSnapshot = $vmSnapshotsListItem.Snapshot
                            break
                        }
                    }

                    # Testing
                    if (!$vmSnapshot)
                    {
                        'Error: No snapshot selected...'
                        '#########################################################################'
                    }
                    else
                    {
                        $null = $vmSourceSnapshotItems.Add($vmSnapshot)
                    }
                }
                while (!$vmSnapshot)
            }
        }



        <#
        Processing
        #>

        Write-Verbose ''
        if ($ItemType -eq 'Snapshot' -and $vmSourceSnapshotItems)
        {
            Write-Verbose ('    - Processing snapshots')
            $vmOrSnapshotSourceItems = $vmSourceSnapshotItems
        }
        elseif ($ItemType -eq 'VM' -and $vmSourceItems)
        {
            Write-Verbose ('    - Processing VMs')
            $vmOrSnapshotSourceItems = $vmSourceItems
        }
        else
        {
            Write-Error 'No object for processing.'
            return
        }

        :vmOrSnapshotSourceItem foreach ($vmOrSnapshotSourceItem in $vmOrSnapshotSourceItems)
        {
            if ($ItemType -eq 'VM')
            {
                $vmOrSnapshotSourceItemNameDisplay = $vmOrSnapshotSourceItem.Name
                $vmOrSnapshotSourceItemName = $vmOrSnapshotSourceItem.Name
            }
            else
            {
                $vmOrSnapshotSourceItemNameDisplay = '{0} - {1}' -f $vmOrSnapshotSourceItem.VMName, $vmOrSnapshotSourceItem.Name

                $snapshotName = $vmOrSnapshotSourceItem.Name
                if ($snapshotName -match ('^{0} - \(.*\)$' -f $vmOrSnapshotSourceItem.VMName)) # Snapshot is in default name
                {
                    $snapshotName = $snapshotName -replace ('^{0} - (.*)' -f $vmOrSnapshotSourceItem.VMName), 'Snapshot $1' # Remove computer name that is in the default snapshot names
                    $snapshotName = $snapshotName -replace '\(|\)|\:| -', ''
                }
                $snapshotName = ($snapshotName -replace '^(.{30}).*', '$1').Trim() # Cut to defined number of characters

                $vmOrSnapshotSourceItemName = '{0} - {1}' -f $vmOrSnapshotSourceItem.VMName, $snapshotName
            }

            Write-Verbose ''
            Write-Verbose ('        - {0}: {1}' -f $ItemType, $vmOrSnapshotSourceItemNameDisplay)



            <#
            Setting parameters
            #>

            Write-Verbose ''
            Write-Verbose '            - Setting parameters:'

            # Name of the target virtual machine
            if ($VmTargetName)
            {
                $vmTargetNameItem = $VmTargetName
            }
            elseif ($AskForVmTargetName)
            {
                while (!$vmTargetNameItem)
                {
                    ''
                    '#########################################################################'
                    ''

                    # Capturing input
                    $vmTargetNameInput = Read-Host 'Please specify name of the target VM (clone)'
                    $vmTargetNameInput = $vmTargetNameInput.Trim()

                    ''
                    'Specified: {0}' -f $vmTargetNameInput
                    ''

                    if ([string]::IsNullOrEmpty($vmTargetNameInput) -or [bool]$(Get-VM -Name $vmTargetNameInput -ErrorAction SilentlyContinue))
                    {
                        '-------------------------------------------------------------------------'
                        ''
                        'VM with this name already exist. Please specify different name.'
                        continue
                    }

                    $vmTargetNameItem = $vmTargetNameInput
                }
            }
            else
            {
                $vmTargetNameItem = '{0} - {1} - Clone - {2}' -f $vmOrSnapshotSourceItemName, $ItemType, (Get-RvDateTimeForOutput)
            }

            Write-Verbose ('                - Target VM name: {0}' -f $vmTargetNameItem)

            while (Get-VM -Name $vmTargetNameItem -ComputerName $ComputerName -ErrorAction SilentlyContinue)
            {
                if ($GenerateUniqueVmTargetName)
                {
                    Write-Verbose ('                    - Already exist. Generating new...')

                    # Unique identifier was already added.
                    if ($vmTargetNameItem -match '.+ - \d+$')
                    {
                        $vmTargetNameItemWithoutUniqueIdentifier = $vmTargetNameItem -replace '(.+) - \d+$', '$1'
                        [int]$vmTargetNameItemUniqueIdentifier = $vmTargetNameItem -replace '.+ - (\d+)$', '$1'
                        ++$vmTargetNameItemUniqueIdentifier
                        $vmTargetNameItem = '{0} - {1}' -f $vmTargetNameItemWithoutUniqueIdentifier, $vmTargetNameItemUniqueIdentifier
                    }

                    # Unique identifier was not added yet.
                    else
                    {
                        $vmTargetNameItem += ' - 1'
                    }


                    Write-Verbose ('                - Target VM name: {0}' -f $vmTargetNameItem)
                }
                else
                {
                    Write-Error ('Error: VM "{0}" already exist. It is possible to have VMs with same name but it is confusing. The whole VM will be skipped.' -f $vmTargetNameItem)
                    continue vmOrSnapshotSourceItem
                }
            }

            # Target virtual machine directory name
            if ($VmTargetDirectoryName)
            {
                $vmTargetDirectoryNameItem = $VmTargetDirectoryName
            }
            else
            {
                $vmTargetDirectoryNameItem = $vmTargetNameItem
            }

            $vmTargetDirectoryNameItem = Remove-RvFileDirectoryIncorrectCharacters -InputObject $vmTargetDirectoryNameItem
            Write-Verbose ('                - Target VM name directory name: {0}' -f $vmTargetDirectoryNameItem)

            # Path to target virtual machine
            $vmTargetDirectoryPath = Join-Path -Path $PathDirectoryVmStorage -ChildPath $vmTargetDirectoryNameItem
            Write-Verbose ('                - Path to the VM''s directory: {0}' -f $vmTargetDirectoryPath)

            if (Get-ChildItem -Path $vmTargetDirectoryPath -ErrorAction SilentlyContinue)
            {
                Write-Error 'Error: Target directory for the VM already exist and it is not empty. It is not good idea to import VM between other files on location. The whole VM will be skipped.'
                continue vmOrSnapshotSourceItem
            }



            <#
            Export
            #>

            Write-Verbose ''
            Write-Verbose '            - Export:'

            # Directory to export
            try
            {
                $pathDirectoryTemporaryDataFull = Join-Path -Path $PathDirectoryTemporaryData `
                    -ChildPath ('Exported VM - Temporary data - {0}' -f (Get-RvDateTimeForOutput -Format 'yyyy-MM-dd HHmmssZ')) `
                    -ErrorAction Stop
                $null = New-Item -Path $pathDirectoryTemporaryDataFull -ItemType directory -ErrorAction Stop
                Write-Verbose ('                - Temporary directory was created: {0}' -f $pathDirectoryTemporaryDataFull)
            }
            catch
            {
                Write-Error 'Error: Cannot create directory to export temporary data. The whole VM will be skipped.'
                continue vmOrSnapshotSourceItem
            }

            # Export
            $exportStartDateTimeUtc = Get-RvDateTime
            Write-Verbose ('                - {0} export in progress... Please wait, it can take a long time.' -f $ItemType)
            Write-Verbose ('                    - Start: {0}' -f (Get-RvDateTimeForOutput -DateTime $exportStartDateTimeUtc))

            if ($Force -or $PSCmdlet.ShouldProcess(('Computer: {0}' -f (Get-RvComputerName -ComputerName $ComputerName -Output DescriptionAndComputerName)), ('Export: {0}: {1}' -f $ItemType, $vmOrSnapshotSourceItemNameDisplay)))
            {
                try
                {
                    if ($ItemType -eq 'VM')
                    {
                        Export-VM -VM $vmOrSnapshotSourceItem -Path $pathDirectoryTemporaryDataFull -ErrorAction Stop
                    }
                    else
                    {
                        Export-VMSnapshot -VMSnapshot $vmOrSnapshotSourceItem -Path $pathDirectoryTemporaryDataFull -ErrorAction Stop
                    }
                }
                catch
                {
                    Write-Error $_
                    Write-Error 'Error: Export failed. The whole VM will be skipped.'
                    continue vmOrSnapshotSourceItem
                }
            }

            $exportEndDateTimeUtc = Get-RvDateTime
            Write-Verbose ('                    - End: {0}' -f (Get-RvDateTimeForOutput -DateTime $exportEndDateTimeUtc))
            Write-Verbose ('                    - Duration: {0}' -f $(Get-RvDateTimeDurationForOutput -Start $exportStartDateTimeUtc -End $exportEndDateTimeUtc))



            <#
            Import
            #>

            Write-Verbose ''
            Write-Verbose '            - Import:'

            # Obtaining path of the configuration file
            Write-Verbose '                - Searching for the configuration files of the exported VM...'
            $vmExportedConfigurationFiles = Get-ChildItem -Path $pathDirectoryTemporaryDataFull -Filter *.xml -Recurse -ErrorAction SilentlyContinue

            $vmExportedConfigurationFilePath = $null
            if ($vmExportedConfigurationFiles.Count -eq 1)
            {
                $vmExportedConfigurationFilePath = $vmExportedConfigurationFiles.FullName
                Write-Verbose '                    - There is one configuration file.'
                Write-Verbose ('                        - Path: {0}' -f $vmExportedConfigurationFilePath)
            }
            elseif ($vmExportedConfigurationFiles.Count -gt 1)
            {
                Write-Verbose '                    - There are multiple configuration files. One of them is probably virtual machine and the other are probably it''s snapshots.'

                foreach ($vmExportedConfigurationFile in $vmExportedConfigurationFiles)
                {
                    if ($vmExportedConfigurationFile.Directory -match '.*\\Virtual Machines$')
                    {
                        $vmExportedConfigurationFilePath = $vmExportedConfigurationFile.FullName
                        break
                    }
                }

                if ($vmExportedConfigurationFilePath)
                {
                    Write-Verbose '                        - Correct file was located.'
                    Write-Verbose ('                            - Path: {0}' -f $vmExportedConfigurationFilePath)
                }
                else
                {
                    Write-Verbose '                        - Error: Cannot locate the correct file'
                }
            }
            else
            {
                Write-Verbose '                    - Error: There is no configuration file.'
            }

            if (!$vmExportedConfigurationFilePath)
            {
                Write-Error 'Error: Cannot locate configuration file for the import. The whole VM will be skipped.'
                continue vmOrSnapshotSourceItem
            }

            # Import
            $importStartDateTimeUtc = Get-RvDateTime
            Write-Verbose ('                - Import in progress... Please wait, it can take a long time.')
            Write-Verbose ('                    - Start: {0}' -f (Get-RvDateTimeForOutput -DateTime $importStartDateTimeUtc))

            try
            {
                $importedVm = Import-VM `
                    -Path $vmExportedConfigurationFilePath `
                    -VirtualMachinePath $vmTargetDirectoryPath `
                    -SmartPagingFilePath $vmTargetDirectoryPath `
                    -SnapshotFilePath $vmTargetDirectoryPath `
                    -VhdDestinationPath (Join-Path -Path $vmTargetDirectoryPath -ChildPath 'Virtual Hard Disks') `
                    -Copy `
                    -GenerateNewId `
                    -ComputerName $ComputerName
            }
            catch
            {
                Write-Error $_
                Write-Error 'Error: Import failed. The whole VM will be skipped.'
                continue vmOrSnapshotSourceItem
            }

            $importEndDateTimeUtc = Get-RvDateTime
            Write-Verbose ('                    - End: {0}' -f (Get-RvDateTimeForOutput -DateTime $importEndDateTimeUtc))
            Write-Verbose ('                    - Duration: {0}' -f $(Get-RvDateTimeDurationForOutput -Start $importStartDateTimeUtc -End $importEndDateTimeUtc))


            <#
            Configuration of the new VM
            #>

            Write-Verbose ''
            Write-Verbose ('                - Configuration of the new VM:')
            Write-Verbose ('                    - Renaming VM to "{0}"...' -f $vmTargetNameItem)
            Rename-VM -VM $importedVm -NewName $vmTargetNameItem -ComputerName $ComputerName



            <#
            Cleaning
            #>

            # Remove exported data
            if ($RemoveTemporaryData)
            {
                Write-Verbose ''
                Write-Verbose ('                - Cleaning:')
                Write-Verbose ('                    - Removing temporary directory with all data: {0}' -f $pathDirectoryTemporaryDataFull)
                try
                {
                    Remove-Item -Path $pathDirectoryTemporaryDataFull -Force -Recurse -ErrorAction Stop
                }
                catch
                {
                    Write-Error 'Error: It is not possible to remove the temporary data.'
                }
            }



            <#
            Configurations
            #>

            if ($Configuration)
            {
                Write-Verbose ''
                Write-Verbose '                - Configurations:'

                # Optimize all VHD and VHDx
                if ($Configuration -contains 'OptimizeDisk')
                {
                    Write-Verbose '                    - Optimize all VHD and VHDx:'
                    $vmDisks = Get-VMHardDiskDrive -VM $importedVm
                    if ($vmDisks)
                    {
                        foreach ($vmDisk in $vmDisks)
                        {
                            Write-Verbose ('                        - Disk: {0}' -f $vmDisk.Name)

                            try
                            {
                                $vmDisk | Optimize-VHD –Mode Quick -ErrorAction Stop
                                Write-Verbose '                            - Result: Sucess'
                            }
                            catch
                            {
                                Write-Verbose '                            - Result: Fail'
                                Write-Error $_
                            }
                        }
                    }
                    else
                    {
                        Write-Verbose '                        - No disks are available.'
                    }
                }

                # Disconnect all NICs from the network
                if ($Configuration -contains 'DisconnectNetwork')
                {
                    Write-Verbose '                    - Disconnect all NICs from the network:'
                    $vmNics = Get-VMNetworkAdapter -VM $importedVm

                    if ($vmNics)
                    {
                        foreach ($vmNic in $vmNics)
                        {
                            Write-Verbose ('                        - NIC: {0}' -f $vmNic.Name)

                            if ($vmNic.Connected)
                            {
                                try
                                {
                                    Disconnect-VMNetworkAdapter -VMNetworkAdapter $vmNic -ErrorAction Stop
                                    Write-Verbose '                            - Result: Sucess'
                                }
                                catch
                                {
                                    Write-Verbose '                            - Result: Fail'
                                    Write-Error $_
                                }
                            }
                            else
                            {
                                Write-Verbose '                            - Result: Already disconnected'
                            }
                        }
                    }
                    else
                    {
                        Write-Verbose '                        - No NICs are available.'
                    }
                }

                # Set all static NIC's MAC addresses to dynamic
                if ($Configuration -contains 'StaticMacToDynamic')
                {
                    Write-Verbose '                    - Set all static NIC''s MAC addresses to dynamic:'
                    $vmNics = Get-VMNetworkAdapter -VM $importedVm

                    if ($vmNics)
                    {
                        foreach ($vmNic in $vmNics)
                        {
                            Write-Verbose ('                        - NIC: {0}' -f $vmNic.Name)

                            if (!$vmNic.DynamicMacAddressEnabled)
                            {
                                try
                                {
                                    Set-VMNetworkAdapter -VMNetworkAdapter $vmNic -DynamicMacAddress -ErrorAction Stop
                                    Write-Verbose '                            - Result: Sucess (set to dynamic)'
                                }
                                catch
                                {
                                    Write-Verbose '                            - Result: Fail'
                                    Write-Error $_
                                }
                            }
                            else
                            {
                                Write-Verbose '                            - Result: Already set to dynamic MAC'
                            }
                        }
                    }
                    else
                    {
                        Write-Verbose '                        - No NICs are available.'
                    }
                }
            }



            <#
            Return
            #>

            $importedVm
        }
    }

    End
    {
        $processesAllEndDateTimeUtc = Get-RvDateTime
        Write-Verbose '    - Processes: All'
        Write-Verbose ('        - End: {0}' -f (Get-RvDateTimeForOutput -DateTime $processesAllEndDateTimeUtc))
        Write-Verbose ('        - Duration: {0}' -f $(Get-RvDateTimeDurationForOutput -Start $processesAllStartDateTimeUtc -End $processesAllEndDateTimeUtc))

        Write-Verbose ''
        Write-Verbose '- Set-RvNetIpAddress: End'
    }
}



Function Clone-RvVmSnapshot
{
    <#
    .SYNOPSIS
        More information in the help for Clone-RvVm function.

    .DESCRIPTION
        Developer
            Developer: Rudolf Vesely, http://rudolfvesely.com/
            Copyright (c) Rudolf Vesely. All rights reserved
            License: Free for private use only

        Description
            More information in the help for Clone-RvVm function.

            Use: Get-Help Clone-RvVm
    #>

    [CmdletBinding(
        SupportsShouldProcess = $true,
        ConfirmImpact = 'Medium'
    )]

    Param
    (
        [Parameter(
            ValueFromPipeline = $true
        )]
        [Microsoft.HyperV.PowerShell.VirtualMachine[]]
        $VmSource,
        [string[]]
        $VmSourceName,
        [Parameter(
            ValueFromPipeline = $true
        )]
        [Microsoft.HyperV.PowerShell.VMSnapshot[]]
        $VmSourceSnapshot,
        [string]
        $VmSourceSnapshotName,
        [string]
        $VmTargetName,
        # [string]
        # $ItemType = 'VM',
        [string]
        $VmTargetDirectoryName,
        [string]
        $PathDirectoryVmStorage = 'C:\VMs',
        [string]
        $PathDirectoryTemporaryData = 'C:\Temp',
        [string]
        $ComputerName = '.',
        [switch]
        $AskForVmTargetName = $false,
        [switch]
        $GenerateUniqueVmTargetName = $true,
        [switch]
        $RemoveTemporaryData = $true,
        [string[]]
        $Configuration = @('OptimizeDisk', 'DisconnectNetwork', 'StaticMacToDynamic'),
        [switch]
        $Force = $false
    )

    Process
    {
        $parametersAndArguments = $PsBoundParameters
        if (![bool]$parametersAndArguments.ItemType) { $parametersAndArguments.Add('ItemType', 'Snapshot') }
        Clone-RvVm @parametersAndArguments
    }
}