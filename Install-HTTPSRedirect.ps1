﻿Start-Process msiexec -ArgumentList '/i "rewrite_amd64.msi" /qn' -Wait

$site = "IIS:\Sites\Default Web Site"
$filterRoot = "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect$_']"
Clear-WebConfiguration -filter $filterRoot
Add-WebConfigurationProperty -pspath $site -filter "system.webServer/rewrite/rules" -name "." -value @{name='HTTP to HTTPS redirect' + $_ ;patternSyntax='Regular Expressions';stopProcessing='False'}
Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/match" -name "url" -value "(.*)"
Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/conditions" -name "logicalGrouping" -value "MatchAll"
Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/conditions" -name "." -value @{
    input = '{HTTPS}'
    matchType ='0'
    pattern ='^OFF$'
    ignoreCase ='True'
}
Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/action" -name "type" -value "Redirect"
Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/action" -name "url" -value "https://{HTTP_HOST}{REQUEST_URI}"