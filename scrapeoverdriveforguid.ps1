# This script takes a csv file with a column heading for bibid and link
# It then queries those urls and pulls out the XPATH location listed
# The output CSV format can then be regex'd into Marc edit and merged with the original file
# This was used to convert link.overdrive.com URLs into clc.lib.overdrive.com URLs for the integration project in Aug 2017
# Created by: Wes Osborn

$csv = Import-Csv "C:\Users\wosborn\Desktop\urlist.csv"
$output = foreach($line in $csv)
{
$bibid = $line.("bibid")
$odurl = $line.("link")
$Request = Invoke-WebRequest $odurl
$Token = ($Request.ParsedHTML.getElementsByName("twitter:image") | Select-Object *).Content
$linedata = $bibid,",",$Token.Substring($Token.IndexOf("{")+1,36),"`n"
$linedata | Out-File "c:\users\wosborn\desktop\guid.csv" -NoNewline -Append
}