# This script will test common TCP ports against CLC's Polaris servers.
# ONLY FAILED servers are reported after each "Testing port NNN..." block
# Should be tested from a client that is experiencing problems
# Copy/paste the results of this script to CLC when requested.
# Created by: Wes Osborn / Updated: 2017-02-18

# REQUIRES Powershell 4+; Windows 8.1+ or Server 2012r2+ to RUN; target OS versions don't matter

# Because DCOM uses dynamic HIGH PORTS, you should test for DCOM connectivity using this script: https://bitbucket.org/clcdpc/powershell-stuff/src/master/testdcomnetworkports.ps1

$BasicHTTPTest = "www.google.com"
# App servers do NOT Support FQDN - so only use hostnames for testing purpoes to mimic the Polaris client
$PolarisDBServers = "proddb","traindb","devdb"
$PolarisAppServers = $PolarisDBServers + "prodapp","prodapp01","prodapp02","trainapp","devapp"
$PolarisSIPServers = "3msip.clcohio.org","prodsip01.clcohio.org","prodsip02.clcohio.org","prodsip03.clcohio.org","prodsip04.clcohio.org","prodsip05.clcohio.org","trainsip.clcohio.org","devsip.clcohio.org"
$PolarisPACServers = "catalog.clcohio.org","trainpac.clcohio.org","devpac.clcohio.org"
$PolarisADServers = "dc45-15.clcdpc.org","dc45-16.clcdpc.org"

$fwDomain = Get-NetFirewallProfile | where name -eq "Domain" | select -expand enabled
$fwPrivate = Get-NetFirewallProfile | where name -eq "Private" | select -expand enabled
$fwPublic = Get-NetFirewallProfile | where name -eq "Public" | select -expand enabled

write-host "Windows Firewall Status - Domain: $fwDomain | Private: $fwPrivate | Public: $fwPublic"
Write-host "External IP: $($(Invoke-RestMethod http://ipinfo.io/json).ip)"
write-host "Local IP: $($(Get-Netipaddress | where AddressFamily -eq "Ipv4").ipaddress -join ', ')"
write-host "Local DNS: $($(Get-DnsClientServerAddress | where AddressFamily -eq 2).ServerAddresses -join ', ')"

write-host ""

Write-host "Testing port 80 at Google"
$BasicHTTPTest | where { -NOT (Test-Netconnection $_ -Port 80 -InformationLevel Quiet)}

Write-host "Testing port 1433"
$PolarisDBServers | where { -NOT (Test-Netconnection $_ -Port 1433 -InformationLevel Quiet)}

Write-host "Testing port 210"
$PolarisAppServers | where { -NOT (Test-Netconnection $_ -Port 210 -InformationLevel Quiet)}

Write-host "Testing port 135"
$PolarisAppServers | where { -NOT (Test-Netconnection $_ -Port 135 -InformationLevel Quiet)}

Write-host "Testing port 13088"
$PolarisAppServers | where { -NOT (Test-Netconnection $_ -Port 13088 -InformationLevel Quiet)}

Write-host "Testing port 5001"
$PolarisSIPServers | where { -NOT (Test-Netconnection $_ -Port 5001 -InformationLevel Quiet)}

Write-host "Testing port 443"
$PolarisPACServers | where { -NOT (Test-Netconnection $_ -Port 443 -InformationLevel Quiet)}

Write-host "Testing port 88"
$PolarisADServers | where { -NOT (Test-Netconnection $_ -Port 88 -InformationLevel Quiet)}

#Write-host "Testing port 445"
#$PolarisADServers | where { -NOT (Test-Netconnection $_ -Port 445 -InformationLevel Quiet)}

Write-host "Testing port 389"
$PolarisADServers | where { -NOT (Test-Netconnection $_ -Port 389 -InformationLevel Quiet)}

Write-host "Testing port 135"
$PolarisADServers | where { -NOT (Test-Netconnection $_ -Port 135 -InformationLevel Quiet)}