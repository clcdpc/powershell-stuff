<# Author: Paul Robson

Description: Creates simple SMB share in server

#>

## Change Name to whatever you want
## Change Path to whatever it needs to be
## Change Full Access accounts if needed
## Change Description
New-SmbShare -Name SIPLogs02 -Path C:\ProgramData\Polaris\5.2\Logs\SIP -ReadAccess Everyone -FullAccess "BUILTIN\Administrators","CLCDPC\Domain Admins" -Description "SIPLogs02"

## Grants access to created SMB
## Add Read of FULL AccessRight to AccountName
Grant-SmbShareAccess -Name SIPLogs02 -AccountName "dwittkop@swpl.org" -AccessRight Read -Confirm:$false

## Check current SMB Shares
Get-SmbShare

## Check who has permission to the share under Name
Get-SmbShareAccess -Name SIPLogs02

## Remove SMB share completely
Remove-SmbShare -Name SIPLogs02 -Confirm:$false

## Revoke SMB permissions individually
Revoke-SmbShareAccess -Name SIPLogs02 -AccountName "mhanna@swpl.org" -Confirm:$false