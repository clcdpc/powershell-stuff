# Recreates the domain trust between the library and the CLC via powershell
# Change the uesrname and password, then execute as a domain admin from a domain controller on the REMOTE (library's) network
# Find more information here: https://discourse.clcohio.org/t/re-building-a-domain-trust/5649
#
# enter an admin account username for CLC - no need to include the domain name
$strRemoteUser = 'a.admin'
# enter your admin account password
$strRemotePassword = 'P@ssw0rd'
# no other changes needed after this line
$localforest = [System.DirectoryServices.ActiveDirectory.Forest]::getCurrentForest() 
$strRemoteForest = 'clcdpc.org'  
$remoteContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', $strRemoteForest,$strRemoteUser,$strRemotePassword) 
$remoteForest = [System.DirectoryServices.ActiveDirectory.Forest]::getForest($remoteContext) 
$localForest.CreateTrustRelationship($remoteForest,'Bidirectional')
# Hit enter key twice after pasting in this script