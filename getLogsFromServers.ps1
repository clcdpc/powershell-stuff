<# Author: Paul Robson

Description: This script can be used to query multiple logs (Event logs/viewer) from multiple workstations/servers.
It will output log entries to a file defined in variable $fileWithLogs.

#>


#Insert in this variable the name of servers from which you want to query logs from
$serversToLog = "ProdPAC","TrainPAC"

#Insert in this variable the name of the different logs you want to query
$logstoOutput = "System","Application","Polaris Diagnostics"

# File path and name where logs are output
$fileWithLogs = "C:\temp\logsQuery.txt"

#Limit log query by entering dates and times
$beforeDate = Get-Date '7/7/17 5:00 PM'
$afterDate = Get-Date '6/30/17 12:00 AM'

foreach ($station in $serversToLog) {
    foreach ($log in $logstoOutput) {
		## This is part of the script that will call up the logs and then output them to the file called "logsQuery.txt"
		## Parameter EntryType provides a way to filter events by their level
		Write-Output $station" - "$log | Out-File -FilePath $fileWithLogs -Append
		Write-Output "==========" | Out-File -FilePath $fileWithLogs -Append
	    Get-EventLog -ComputerName $station -LogName $log -EntryType Warning,Error -after $afterDate -Before $beforeDate | Select TimeGenerated,EntryType,Source,EventID,Message | Out-File -FilePath $fileWithLogs -Append
    }
}