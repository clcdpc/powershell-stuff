# Copies Polaris Offline files to local machines. 

param (
    [bool]$PolarisClient = $true, #Setting this to true will excute checks to make sure the local client is properly setup; if server use $false
    [string]$SourceFileHost="proddb.clcdpc.org", #Host server with Offline Files; it is preferable to make one copy from CLC and then have your clients use a locally hosted copy
    [string]$OfflineFileShareName="Offline", #Name of share on Polaris or local server that contains the files you want copied
    [string]$LogFilePath="c:\LogFiles", #Location where you'd like to store log files
    [string]$PolarisFolderBase="C:\ProgramData\Polaris", #Base location where local machine is configured to store offline files
    [string]$LocalCopyLocation="C:\PolarisOffline" #ONLY used when using the script to copy to a local server; requires setting $IsPolarisClient=$false
)

# If copying the files to a local client, there should be an Offline folder and we should use that as the destination
if($PolarisClient -eq $true){
    #Queries the ProgramData folder location for the highest Polaris version number
    $OfflineFolderBase = Get-ChildItem $PolarisFolderBase | Where-Object{ $_.PSIsContainer } | Sort-Object { $_.FullName } | Select-Object -ExpandProperty FullName -Last 1
    $OfflineFileDest = "$OfflineFolderBase\Offline\"
# If copying the files to a server, accept whatever destination location was provided
} else {
    $OfflineFileDest = $LocalCopyLocation
}

$checkoffline = Test-Path -PathType Container "$OfflineFileDest"
if($checkoffline -eq $false){
    Write-host "Offline folder $OfflineFileDest location not properly configured. Please check Polaris or script configuration. Exiting."
    exit
}

$checklog = Test-Path -PathType Container "$LogFilePath"
if($checklog -eq $false){
    New-Item $LogFilePath -type Directory
}

$LogFile="$LogFilePath\FetchFileLog.txt"

If (Test-Path $LogFile){
	Remove-Item $LogFile
}

Get-Date >> $LogFile
If (Test-Connection $SourceFileHost -quiet) {Copy-Item \\$SourceFileHost\$OfflineFileShareName\*.* $OfflineFileDest -force}
"Offline Files Copied to Destination" >> $LogFile

# change to offline file location for easier testing when debugging the script
Set-Location $OfflineFileDest