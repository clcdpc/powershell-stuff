<#
.SYNOPSIS
Script runs a check on computers for OS description and whether it has been activated or not

.DESCRIPTION
Script checks and shows OS description as well as whether it has been activated or not.

.PARAMETER ComputerName
Name of hypervisor to query for checkpoints / snapshots

.PARAMETER printToFile
This gives the option to user whether to save results to a file or just to screen.
Options are Yes or No.

#>
Function Get-OSActivation
{
	[CmdletBinding()]
    Param(
		[Parameter(Mandatory=$true)]
		[String[]]$ComputerName,
		
		[Parameter(Mandatory=$false)]
		[ValidateSet("Yes","No")]
		[String]$printToFile
	)
		$pathToPrint = "C:\temp"
		$fileName = "OS-ActivationStatus.txt"
		$LicenseStatus = @("Unlicensed","Licensed","OOB Grace","OOT Grace","Non-Genuine Grace","Notification","Extended Grace","Unknown-Offline")
		
		$results = @()
		
		foreach ($pc in $ComputerName) {
			#Write-Host $pc
			$wrksttonLcns = Get-CimInstance -Classname SoftwareLicensingProduct -ComputerName $pc `
			        -Filter "ApplicationID = '55c92734-d682-4d71-983e-d6ec3f16059f'" -ErrorAction Ignore -Property LicenseStatus,ID `
			        | where-object { $_.ID -eq '641f81b2-63c2-47dd-aba7-c24bf651ff85' -or ` #WinSrvr2012R2
                               $_.ID -eq 'fea51083-1906-44ed-9072-86af9be7ab9a' -or ` #WinSrvr2016
                               $_.ID -eq '2ffd8952-423e-4903-b993-72a1aa44cf82' }  #Win10
			#Write-Host $wrksttonLcns
			
			$theOS = Get-CimInstance -classname Win32_OperatingSystem -computername $pc -ErrorAction Ignore | Select-object -ExpandProperty Caption
			#Write-Host $theOS
			
			if (($wrksttonLcns -eq $null) -or ($theOS -eq $null)) {
                    $wrksttonLcns = New-Object psobject -Property @{ 'LicenseStatus' = 7; }
                    $theOS = "Unknown - Offline"
                    }
					
			$results += New-Object psobject -ErrorAction Ignore -Property @{
				    'ComputerName' = $pc;
				    'OSEdition' = $theOS;
				    'Status' = $LicenseStatus[$wrksttonLcns.LicenseStatus];
				}
		}
		
		if ($printToFile -match "yes") {
		
			if (!(Test-Path -Path $pathToPrint)) {
				New-Item -Path $pathToPrint -ItemType Directory
			}
            else {
			    New-Item -Path "$pathToPrint\$fileName" -ItemType File
		
			    $results
			    $results | Out-file -filepath "$pathToPrint\$fileName"
				Write-Host "Results have been written to $pathToPrint\$fileName"
            }
		}
		
		Else { $results }
	
}