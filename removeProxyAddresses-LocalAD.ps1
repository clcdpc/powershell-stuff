<# Author: Paul Robson

Created on Feb 28, 2017

Description: Searches for any users under specific OU (SearchBase) for any proxyAddresses attributes,
it then stores the samAccountName and lists in variable.
The loop will then clear any smtp or x500 proxyAddresses

#>

$aduser = Get-ADUser -Filter 'Name -like "*"' -SearchBase 'OU=CLCExchangeUsers,DC=clcdpc,DC=org' -Properties proxyAddresses | where proxyAddresses -ne $null | select samaccountname

foreach ($user in $aduser){
    Get-ADUser $user.samaccountname -Properties proxyAddresses | % {
	$pr1 = $_.proxyAddresses -like "x500*"
    $pr2 = $_.proxyAddresses -like "smtp*"
	Set-ADUser $_ -Remove @{proxyaddresses=$pr1}
    Set-ADUser $_ -Remove @{proxyaddresses=$pr2}
	#Adding alias proxyAddress - NOTE change domain
	#Set-ADUser $_ -add @{proxyAddresses="SMTP:"+ $_.GivenName + '.' + $_.Surname +"@clcohio.org"}
    }
    Get-ADUser $user.samaccountname -Properties proxyAddresses | select name,proxyAddresses
}