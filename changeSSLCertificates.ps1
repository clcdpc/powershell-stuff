<# Author: Paul Robson

Description: Change SSL certificates

#>

--View Current SSL certificate
netsh http show sslcert
-- Set password to unlock certificate as it is imported
$mypwd = ConvertTo-SecureString -String "000000" -Force -AsPlainText
-- Import PFX certificate into Personal certificate store
Import-PfxCertificate -FilePath C:\temp\<name of cert>.pfx cert:\localMachine\my -Password $mypwd
-- List all certificates in Personal Store and copy thumbprint of OLD certificate
Get-ChildItem Cert:\LocalMachine\My
-- Remove old certificate (paste it into cmdlet below)
Get-ChildItem Cert:\LocalMachine\My\<insert copied thumbprint here>
-- Remove SSL certificate binding with old cert
netsh http delete sslcert ipport=0.0.0.0:443
-- ADD the new SSL certificate via netsh command
$command = @'
netsh http add sslcert ipport=0.0.0.0:443 certhash=<thumbprint of NEW cert> appid="{1d40ebc7-1983-4ac5-82aa-1e17a7ae9a0e}"
'@
Invoke-Expression -Command:$command