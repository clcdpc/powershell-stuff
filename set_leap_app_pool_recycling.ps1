﻿Import-Module WebAdministration
$appPools = "Polaris Application Services", "Polaris Web Client"

foreach($appPool in $appPools){
	Set-ItemProperty -Path "IIS:\AppPools\$appPool" -Name Recycling.periodicRestart.schedule -Value @{value="01:00"}
	Set-ItemProperty -Path "IIS:\AppPools\$appPool" -Name Recycling.periodicRestart.time -Value '0'
}