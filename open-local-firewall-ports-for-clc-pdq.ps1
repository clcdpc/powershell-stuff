Write-host "Enabling CLC PDQ firewall ports"

New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 137" –Direction inbound -RemoteAddress 192.168.144.0/24 –LocalPort 137 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 138" –Direction inbound -RemoteAddress 192.168.144.0/24 –LocalPort 138 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 445" –Direction inbound -RemoteAddress 192.168.144.0/24 –LocalPort 445 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC TCP Port 139" –Direction inbound -RemoteAddress 192.168.144.0/24 –LocalPort 139 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC TCP Port 445" –Direction inbound -RemoteAddress 192.168.144.0/24 –LocalPort 445 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 137" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 137 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 138" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 138 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC UDP Port 445" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 445 -Protocol UDP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC TCP Port 139" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 139 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC TCP Port 445" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 445 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "Allow inbound from CLC TCP Port 6336" –Direction inbound -RemoteAddress 192.168.44.0/23 –LocalPort 6336 -Protocol TCP -Action Allow