<#
.SYNOPSIS
Replaces ERMS executable

.DESCRIPTION
The script looks for servers that start with Dev*, Train*, or Prod* . Then looks into those servers and narrows down to list only the
ones running the ERMS service.
After that, in each of those servers multiple services are stopped because ERMS depends on them. The script checks status of those
services.
The old ERMSDaemon.exe is removed, and the new one is copied from the patched one in DEVDB.
Then all the services are restarted.

.PARAMETER Log
The name of the log to get events from.

#>

$ERMSService = "ERMSDaemon.5.6"
$ERMSPatch = "\\devdb\Build5.6.125.0\17DecPolarisERMS\ERMSDaemon.exe"
$PolarisERMS = "C:\Program Files\Polaris\5.6\Bin\ERMSDaemon.exe"

$whereiserms = Get-ADComputer -Filter * -SearchBase 'OU=CLC Polaris Servers, DC=clcdpc, DC=org' `
    | where {$_.DNSHostname -like 'Dev*' -and $_.DNSHostname -like 'Train*' -and $_.DNSHostname -like 'Prod*'} `
    | select -ExpandProperty DNSHostName

# Remotely get servers that are running the ERMS service

$serverswitherms = Invoke-Command -ComputerName $whereiserms -ScriptBlock { Get-Service -Name $ERMSService -ErrorAction SilentlyContinue } `
        | Select -ExpandProperty PSComputerName

foreach ($srvr in $serverswitherms) {

	## Command stops ERMS and all other services that it depends on
    Invoke-Command -ComputerName $srvr -ScriptBlock { Stop-Service WinHttpAutoProxySvc,netprofm,NlaSvc,Dhcp,EventLog,ERMSDaemon.5.6 `
        -Force -ErrorAction SilentlyContinue -Verbose }

	## Command checks status of those services previously stopped
    Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Service -Name Dhcp,EventLog,WinHttpAutoProxySvc,netprofm,NlaSvc,ERMSDaemon.5.6 `
		-ErrorAction SilentlyContinue }

	## Old ERMSDaemon.exe is removed
    Invoke-Command -ComputerName $srvr -ScriptBlock { Get-Item -Path $Using:PolarisERMS | Remove-Item -Verbose }

	## ERMS patch is copied from DEVDB to local Polaris 5.6 directory
    Copy-Item -Path $ERMSPatch -Destination "\\$srvr\c$\Program Files\Polaris\5.6\Bin" -Verbose

	## Command restarts new all services previously stopped
    Invoke-Command -ComputerName $srvr -ScriptBlock { Start-Service WinHttpAutoProxySvc,netprofm,NlaSvc,Dhcp,EventLog,ERMSDaemon.5.6 `
		-ErrorAction SilentlyContinue -Verbose }
}
