<#
.SYNOPSIS
Removes VM Checkpoints / snapshots older than 180 days

.DESCRIPTION
Removes VM Checkpoints / snapshots older than 180 days.

.PARAMETER olderThanDays
Enter the number of days. It will only show you the snapshots older than this number of days.

.PARAMETER ComputerName
Name of hypervisor to query for checkpoints / snapshots

#>
Function Remove-CheckPoints
{
	[CmdletBinding()]
    Param(
		
		[String[]]
        $Computername = ".",
		
		[Int]
        $olderThanDays = 180
	)
	ForEach ($Name in $Computername)
    {
		Write-Verbose "Searching for VM checkpoints according to the number of days"
		$checkpointsRM = `
		Invoke-Command -ComputerName $Name `
			-ScriptBlock { Get-VM | Get-VMSnapshot | Where-Object {$_.CreationTime -lt (Get-Date).AddDays(-$Using:olderThanDays) } `
			| Select VMName,Name,CreationTime }
			
		ForEach ($pcname in $checkpointsRM)
		{
			$currentVM = ($pcname.VMName)
			Write-Verbose "Removing checkpoints for VM $currentVM"
			Invoke-Command -ComputerName $pcname.PSComputerName `
				-ScriptBlock { Remove-VMSnapshot -VMName $Using:pcname.VMName -Name $Using:pcname.Name }
		}
	}
	
}