$hosts = @("hyperv07","hyperv08","hyperv09")

ForEach ($vmhost in $hosts) {

    write-host "----$vmhost----"
    $VMs = Get-VM -computername $vmhost | where-object "name" -notlike "*replica*" | Sort-Object Name
    $TimeSync = Get-VM -computername $vmhost | Select-Object -ExpandProperty VMIntegrationService
    ForEach ($VM in $VMs) {
    $TimeSyncEnabled = ($TimeSync | Where-Object {$_.VMName -like $($VM.Name) -and $_.Name -like "Time Synchronization"}) | Select-Object -ExpandProperty Enabled
    if ($TimeSyncEnabled -eq "True") {write-host "Time Synchornization enabled on $($VM.Name)"} else {"Time Synchornization NOT enabled on $($VM.Name)"}

    }
}