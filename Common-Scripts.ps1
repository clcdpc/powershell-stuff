Get-Module | Remove-Module

Function Test-Admin {
    if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
        if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
         write-warning "You appear to be running as a standard user. Please run as Administrator."
         Start-Sleep 60
         exit
} 
} else {write-output "Congratulations, you are running as Administrator."}
}

Function Test-Ping ($computer = "localhost") {
  $ping = new-object System.Net.NetworkInformation.Ping
  Try  {  [void]$ping.send($computer,1) ;  $Online = $true   }
  Catch  {    $Online = $False  }
  Return $Online
}

Function Find-ScheduledTasks ($author) {
#Queries OUs that contain server devices and uses DNS host names to create CIM sessions. Once cimsessions are created, it searches for scheduled tasks and then displays them. NOTE: Some servers, Windows Server 2012 and older do not connect via Powershell for some reason.
$ping = new-object System.Net.NetworkInformation.Ping

$serverous = "OU=CLC Misc Servers (Non-DC),DC=clcdpc,DC=org","OU=CLC HyperV Hosts,DC=clcdpc,DC=org","OU=CLC Polaris Servers,DC=clcdpc,DC=org",
    "OU=CLC SCCM,DC=clcdpc,DC=org","OU=Domain Controllers,DC=clcdpc,DC=org","OU=RemoteDesktopServers,DC=clcdpc,DC=org"

$servers = @()
$servers = $serverous | ForEach-Object { Get-ADComputer -Filter * -SearchBase $_ } | Select-Object -ExpandProperty DNSHostName

$onlineservers = @()
foreach ($server in $servers) {
if ($ping.send($server,1).status -eq "Success") { $onlineservers += $server }   
}

$newservers = @()
foreach ($server in $onlineservers) {
$os = try {(Get-WmiObject Win32_OperatingSystem -ComputerName $server -ErrorAction "Stop").Caption} catch {"error"}
if ($os -like "*2016*") {$newservers += $server}
elseif ($os -like "*2012 R2*") {$newservers += $server}
elseif ($os -like "error") {$newservers += $server}
else {write-output "$server appears to be running $os, no good."}
}

$cim = try {New-CimSession -ComputerName $newservers -Name hvhosts -ErrorAction "Stop"} catch {}

$tasks = @()
$tasks = Get-ScheduledTask -CimSession $cim | Where-Object {($_.author -like "*$author*") -and ($_.TaskName -notlike "*User_Feed_Sync*") }

foreach ($task in $tasks) {

    $task | Sort-Object -Descending $_.PSComputerName | Select-Object -Property `
        @{Name="Target Device";Expression={$_.PSComputername}},
        @{Name="Task Name";Expression={$_.TaskName}},
        @{Name="Task Author";Expression={$_.Author}},
        @{Name="Task Runner";Expression={$_.Principal.UserId}}
}

}

Function Find-DateTimeDCs {
ForEach ($DC in $(Get-addomaincontroller -Filter *)) { $DC.Name + '  ' + ([WMI]'').ConvertToDateTime((Get-WmiObject win32_operatingsystem -computername $DC.Name).LocalDateTime) }
}

Function Start-DNSCacheFlush {
test-admin
ForEach ($DC in $(Get-ADDomainController -Filter *)) {Clear-DnsServerCache -ComputerName $DC.Name -Force}
write-output "Successfully flushed DNS cache on all Domain Controllers"
Clear-DnsClientCache
write-output "Successfully flushed DNS cache on $($env:computername)"
}

Function Start-ADReplication {
Start-DNSCacheFlush
ForEach ($DC in Get-ADDomainController -Filter *) {REPADMIN /syncall /A /e /q $DC.Name}
}

Function Start-GPUpdate ($context) {
test-admin
switch ($context) {
'DC' {$Searchbase = "OU=Domain Controllers,DC=clcdpc,DC=org"; break}
'Polaris' {$Searchbase = "OU=CLC Polaris Servers,DC=clcdpc,DC=org"; break}
'PolarisProd' {$Searchbase = "OU=Production,OU=CLC Polaris Servers,DC=clcdpc,DC=org"; break}
'PolarisDev' {$Searchbase = "OU=Development,OU=CLC Polaris Servers,DC=clcdpc,DC=org"; break}
'PolarisTrain' {$Searchbase = "OU=Training,OU=CLC Polaris Servers,DC=clcdpc,DC=org"; break}
'Misc' {$Searchbase = "OU=CLC Misc Servers (Non-DC),DC=clcdpc,DC=org"; break}
'PCs' {$Searchbase = "OU=CLC PCs,DC=clcdpc,DC=org"; break}
'Alex' {$Searchbase = "OU=ALE Computers,DC=clcdpc,DC=org"; break}
'HyperV' {$Searchbase = "OU=CLC HyperV Hosts,DC=clcdpc,DC=org"; break}
'RDS' {$Searchbase = "OU=RemoteDesktopServers,DC=clcdpc,DC=org"; break}
}

$targets = Get-ADComputer -Filter * -SearchBase $Searchbase | Select -ExpandProperty DNSHostName | Sort

foreach ($target in $targets) {
if (test-ping $target) {
Write-Host -NoNewline "Attempting GPUpdate on $target..."
try {Invoke-GPUpdate -Computer $target -ea "Stop"; Write-Host "done!"} catch {write-host "failed!"}
} else {write-warning "$target offline!"}
}
}

Function Restart-PolConsumerServices ($server) {
test-admin
$('PolAPIConsumerService','PolAPIConsumerServiceOvernightProc').ForEach({
Get-Service -ComputerName $server -name $_ | Restart-Service
Get-Service -ComputerName $server -name $_
})
}

Function Restart-SIP ($servers) {
test-admin
ForEach ($server in $servers) {
$('Polaris SIP').ForEach({
Get-Service -ComputerName $Server -Name $_ | Restart-Service
Get-Service -ComputerName $Server -Name $_
})}
}

Function Show-SPNs {

$search = New-Object DirectoryServices.DirectorySearcher([ADSI]"")
$search.filter = "(servicePrincipalName=*)"
$results = $search.Findall()

foreach($result in $results)
{
       $userEntry = $result.GetDirectoryEntry()
       Write-host "Object Name = " $userEntry.name -backgroundcolor "yellow" -foregroundcolor "black"
       Write-host "DN      =      "  $userEntry.distinguishedName
       Write-host "Object Cat. = "  $userEntry.objectCategory
       Write-host "servicePrincipalNames"
       $i=1
       foreach($SPN in $userEntry.servicePrincipalName)
       {
           Write-host "SPN(" $i ")   =      " $SPN       $i+=1
       }
       Write-host ""

}
}

Function Restart-PACServices ($server) {
test-admin
Invoke-Command -ComputerName $server -ScriptBlock {

$comAdmin = New-Object -com ("COMAdmin.COMAdminCatalog.1")
$applications = $comAdmin.GetCollection("Applications") 
$applications.Populate()
$app1 = $applications | Where-Object {$_.Name -like "Polaris*" }
    
foreach ($application in $applications)
{
    
    $components = $applications.GetCollection("Components",$application.Key)
    $components.Populate()
     foreach ($component in $components)
    {
           if ( $component.Name -like "Polaris*" )
           {
            $dllName = $component.Value("DLL")
            $componentName = $component.Name
    
            "Component Name:$componentName"
            "DllName: $dllName`n"
          }
    }
}
    
$comAdmin.ShutdownApplication($app1.Name)
$comAdmin.StartApplication($app1.Name)
    
$ERMSSService = Get-Service -Name "ERMS*"
    
Stop-Service $ERMSSService -Force
Start-Service $ERMSSService
    
iisreset

}

}

Function Test-PolarisPorts {

$BasicHTTPTest = "www.google.com"
# App servers do NOT Support FQDN - so only use hostnames for testing purposes to mimic the Polaris client
$PolarisDBServers = "proddb","traindb","devdb"
$PolarisAppServers = $PolarisDBServers + "prodapp","prodapp01","prodapp02","trainapp","devapp"
$PolarisSIPServers = "3msip.clcohio.org","prodsip01.clcohio.org","prodsip02.clcohio.org","prodsip03.clcohio.org","prodsip04.clcohio.org","prodsip05.clcohio.org","trainsip.clcohio.org","devsip.clcohio.org"
$PolarisPACServers = "catalog.clcohio.org","trainpac.clcohio.org","devpac.clcohio.org"
$PolarisADServers = "dc45-15.clcdpc.org","dc45-16.clcdpc.org"


$fwDomain = Get-NetFirewallProfile | where name -eq "Domain" | select -expand enabled
$fwPrivate = Get-NetFirewallProfile | where name -eq "Private" | select -expand enabled
$fwPublic = Get-NetFirewallProfile | where name -eq "Public" | select -expand enabled

write-host "Windows Firewall Status - Domain: $fwDomain | Private: $fwPrivate | Public: $fwPublic"
Write-host "External IP: $($(Invoke-RestMethod http://ipinfo.io/json).ip)"
write-host "Local IP: $($(Get-Netipaddress | where AddressFamily -eq "Ipv4").ipaddress -join ', ')"
write-host "Local DNS: $($(Get-DnsClientServerAddress | where AddressFamily -eq 2).ServerAddresses -join ', ')"

write-host ""

Write-host "HTTP test: Testing port 80 at Google"
$BasicHTTPTest | Where-Object { -NOT (Test-Netconnection $_ -Port 80 -InformationLevel Quiet)}

Write-host "DB servers test: Testing port 1433"
$PolarisDBServers | Where-Object { -NOT (Test-Netconnection $_ -Port 1433 -InformationLevel Quiet)}

$ports = @('210','135','13088')
Foreach ($port in $ports) {
Write-host "App Servers test: Testing port $port"
$PolarisAppServers | Where-Object { -NOT (Test-Netconnection $_ -Port $port -InformationLevel Quiet)}
}

Write-host "SIP servers test: Testing port 5001"
$PolarisSIPServers | Where-Object { -NOT (Test-Netconnection $_ -Port 5001 -InformationLevel Quiet)}

Write-host "PAC servers test: Testing port 443"
$PolarisPACServers | Where-Object { -NOT (Test-Netconnection $_ -Port 443 -InformationLevel Quiet)}

$ports = @('88','389','135')
ForEach ($port in $ports) {
Write-host "AD test: Testing port $port"
$PolarisADServers | Where-Object { -NOT (Test-Netconnection $_ -Port $port -InformationLevel Quiet)}
}


}

Function Test-Command ($Command) {
    Try  {
        Get-command $command -ErrorAction Stop
        Return $True
    }

    Catch [System.SystemException]  {
        Return $False
    }
}

Function Connect-Exchange {

IF (Test-Command "Get-Mailbox") {Write-Host "Exchange cmdlets already present"}
Else {
    $CallEMS = ". '$env:ExchangeInstallPath\bin\RemoteExchange.ps1'; Connect-ExchangeServer -auto -ClientApplication:ManagementShell "
    Invoke-Expression $CallEMS
}}

Function Connect-O365 {

IF (Test-Command "Get-Mailbox") {Write-Host "Exchange cmdlets already present"}

Import-Module $((Get-ChildItem -Path $($env:LOCALAPPDATA+"\Apps\2.0\") -Filter Microsoft.Exchange.Management.ExoPowershellModule.dll -Recurse ).FullName|Where-Object{$_ -notmatch "_none_"}|Select-Object -First 1)
$EXOSession = New-ExoPSSession -UserPrincipalName $(([ADSI]"LDAP://<SID=$([System.Security.Principal.WindowsIdentity]::GetCurrent().User.Value)>").UserPrincipalName)
Import-PSSession $EXOSession 

}

Function New-VM {

param (
    [Parameter(Position=0,mandatory=$true)][string]$name, #VM Name
    [Parameter(Position=0,mandatory=$true)][string]$hvhost, #Hypervisor
    [Parameter(Position=0,mandatory=$true)][string]$IP,
    $edition = "GUI", #GUI or Core
    $RAM = 8, #Gigabytes
    $CPU = 8, #Cores
    $TimeSync = $true #Time Sync should be enabled for non-DCs
    
)

Test-Admin

Invoke-Command -ComputerName $hvhost -ArgumentList $name, $hvhost, $IP, $edition, $RAM, $CPU, $TimeSync -ScriptBlock {
param ($name, $hvhost, $IP, $edition, $RAM, $CPU, $TimeSync)

$VLAN = $($IP.split("."))[2]
$RAM = [int64]$RAM * 1GB

write-output "VM Host: $hvhost"
write-output "VM Name: $name"
write-output "Edition: $edition"
write-output "Memory:  $RAM bytes"
write-output "Cores:   $CPU"
write-output "VLAN:    $VLAN"
write-output "IP:      $IP"
write-output ""

## Variables that stay relatively static

$defaultVHDPath=(Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Virtualization'  -Name 'DefaultVirtualHardDiskPath').DefaultVirtualHardDiskPath 
$defaultVMPath=(Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Virtualization'  -Name 'DefaultExternalDataRoot').DefaultExternalDataRoot
if (test-path "r:\vmtemplates") {$templatePath = "r:\vmtemplates"} else {$templatePath = "\\clcdpc.org\files\CLCStaff\vmtemplates"} #VM Resources, locally if possible
$newVMPath = "$defaultVMPath\$name"
$newVHD = "$defaultVMPath\$name\$name.vhdx"
$currentVMSwitch = "LanVirtualSwitch"

## Create directory of where the new VM will live, copy VHD, create VM
if (!(Test-Path "$defaultVMPath\$name")) {mkdir "$defaultVMPath\$name" | Out-Null}

switch ($edition) {
"GUI" {$baseVHD = "win2016FullGUI.vhdx"}
"Core" {$baseVHD = "WinSrvr2016-Core.vhdx"}
}

write-host "$edition selected, copying $templatePath\$baseVHD to $newVHD"
Copy-Item $templatePath\$baseVHD $newVHD -ea Stop
write-host ""

write-host "Creating VM $name"
New-VM -Name $name -MemoryStartupBytes $RAM -VHDPath $newVHD -Path $defaultVMPath -Generation 2 | Out-Null
Get-VM -Name $name | Set-VMProcessor -Count $CPU | Out-Null


## Generate Basic OS settings

$AdminPassword='WWeellccoommee!!'
$Organization='Central Library Consortium'
$Owner='Central Library Consortium'
$TimeZone='Eastern Standard Time'

write-host "Configuring Unattend file"
$UnattendTemplate="$templatePath\unattend-template.xml" 
$Unattendfile=New-Object XML 
$Unattendfile.Load($UnattendTemplate) 
$Unattendfile.unattend.settings.component[0].ComputerName=$name
$Unattendfile.unattend.settings.component[0].RegisteredOrganization=$Organization 
$Unattendfile.unattend.settings.component[0].RegisteredOwner=$Owner 
$Unattendfile.unattend.settings.component[0].TimeZone=$TimeZone 
$Unattendfile.unattend.settings.Component[1].RegisteredOrganization=$Organization 
$Unattendfile.unattend.settings.Component[1].RegisteredOwner=$Owner 
$UnattendFile.unattend.settings.component[1].UserAccounts.AdministratorPassword.Value=$AdminPassword
$UnattendFile.unattend.settings.component[1].autologon.password.value=$AdminPassword 
 
$UnattendXML=$templatePath+"\unattend.xml" 
$Unattendfile.save($UnattendXML)

## Apply Basic OS settings
Mount-DiskImage -ImagePath $newVHD -StorageType VHDX #Mounts the VHD
$drvLetter = (Get-DiskImage -ImagePath $newVHD | Get-Disk | Get-Partition).DriveLetter.Get(3)+":" #Sets VHD drive letter
write-output "$newVHD mounted to $drvLetter"
$Destination1 = $drvLetter+"\Windows\System32\Sysprep\unattend.xml" # Sets destination of XML inside the VHDX
$Destination2 = $drvLetter+"\Windows\Panther\unattend.xml" # Sets destination of XML inside the VHDX

write-host "Copying script files"
## Sets and creates folder where PS scripts will go
$ScriptFolder = $drvLetter+"\ProgramData\Scripts\" 
New-Item $ScriptFolder -ItemType Directory | Out-Null

## Copies Needed files
Copy-Item "$templatePath\FirstStart.cmd" $ScriptFolder #launcher for First Startup script
Copy-Item "$templatePath\SecondStart.cmd" $ScriptFolder #launcher for Second Startup script
Copy-Item "$templatePath\prod-adCreds.csv" $ScriptFolder #creds to join domain
Copy-Item "$templatePath\FirstStart.ps1" $ScriptFolder #First Startup script
Copy-Item "$templatePath\SecondStart.ps1" $ScriptFolder #Second Startup script
Copy-Item $UnattendXML -Destination ("$defaultVMPath\$name\unattend.xml") | Out-Null #Keep a copy of unattend file for reference
Copy-Item $UnattendXML $Destination1 #unattend
Copy-Item $UnattendXML $Destination2 #unattend

write-host "Updating FirstStart.cmd with parameters"

$baseFirstStart = Get-Content "$ScriptFolder\FirstStart.cmd" -Raw -Encoding OEM
$newFirstStart = "$baseFirstStart -IP $IP -VLAN $VLAN"
$newFirstStart | Out-File "$ScriptFolder\FirstStart.cmd" -Encoding OEM

## Opens registry path, and sets FirstStart.cmd to run on first startup

$RemoteReg = $drvLetter+"\Windows\System32\config\Software"
REG LOAD "HKLM\REMOTEPC" $RemoteReg | Out-Null
NEW-ITEMPROPERTY "HKLM:REMOTEPC\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "PoshStart" -Value "C:\ProgramData\Scripts\FirstStart.cmd" | Out-Null
REG UNLOAD "HKLM\REMOTEPC" | Out-Null

## Dismounts new VM's VHDX
dismount-diskimage -ImagePath $newVHD | Out-Null

## Hyper-V Settings

write-host "Configuring Hyper-V network settings"
## Connect new VM to VM Switch
Connect-VMNetworkAdapter -VMName $name -SwitchName $currentVMSwitch #Connect to Switch

## Set VM network adapter for a specific VM to use Access mode to a certain VLAN #Set VLAN
Set-VMNetworkAdapterVlan -VMName $name -Access -VlanId $VLAN
Set-VMNetworkAdapter -VMName $name -IovWeight 100 #SR-IOV

## Enable Time Sync service on a VM. This needs to be enabled on all non-DCs
if ($timesync) {Enable-VMIntegrationService -VMName $name -name "Time Synchronization"}
Enable-VMIntegrationService -VMName $name -name "Guest Service Interface"

## Starts the new VM
write-host "Starting VM $name"
Start-VM $name

#Clean up
Remove-Item $UnattendXML -Force
}

vmconnect.exe $hvhost $name
}