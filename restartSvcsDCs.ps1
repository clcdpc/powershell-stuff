<# Author: Paul Robson

Description: This restarts three services in Windows Server 2012 R2 because Altaro has problems with it
after VM is restarted


BEFORE RUNNING SCRIPT
You need to enter user email account below
#>

$dcsneedservices = "DC10-11","DC10-16","DC44-11","DC44-12","DC45-11","DC45-12","DC144-11","DC144-12"
#$dcsneedservices = "DC144-11.clcdpc.org"

## Request passwords and turns it into a secure string
#Read-Host -Prompt 'Enter Pass here' -AsSecureString | ConvertFrom-SecureString | Out-File "c:\programdata\clcTestRestart\sec-string.txt"

$luser = "CLCDPC\a.probson"
$lpassword = Invoke-Command { Get-Content "c:\programdata\clcTestRestart\sec-string.txt" | ConvertTo-SecureString }
$lpassword = Read-Host -Prompt 'Enter Password here' -AsSecureString
$usersession = new-object -typename System.Management.Automation.PSCredential -argumentlist $luser, $lpassword
$usersession = Get-Credential -Credential "CLCDPC\a.probson"

$computers = "testdc02","testws2016"

foreach ($dcs in $dcsneedservices) {
    
    $dcs

    #Restart-VM -Name $dcs -Force -Verbose

    #Wait-VM -Name $dcs -Delay 180 -Verbose

    Invoke-Command $dcs -Credential $usersession { Restart-Service -Name VSS -Verbose } -Verbose

    Invoke-Command $dcs -Credential $usersession { Restart-Service -Name EventSystem -Verbose -Force } -Verbose

    Invoke-Command $dcs -Credential $usersession { Restart-Service -Name COMSysApp -Verbose -Force } -Verbose

    #Invoke-Command $dcs -Credential $usersession { Get-Service -Name VSS } -Verbose
    
    #Invoke-Command $dcs -Credential $usersession { Get-Service -Name EventSystem -Verbose } -Verbose

    #Invoke-Command $dcs -Credential $usersession { Get-Service -Name COMSysApp -Verbose } -Verbose

}
