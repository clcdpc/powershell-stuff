Get-NetAdapter

$description = (Get-NetAdapter -Name "Ethernet").InterfaceDescription

Get-NetAdapterBinding -InterfaceDescription $description | Select-Object Name,DisplayName,ComponentID

Disable-NetAdapterBinding -InterfaceDescription $description -ComponentID ms_tcpip6

Enable-NetAdapterBinding -InterfaceDescription $description -ComponentID ms_tcpip6