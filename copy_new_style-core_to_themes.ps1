﻿$themeDirs = @("alexandria"
              ,"delaware"
              ,"fairfield"
              ,"grandview"
              ,"granville"
              ,"london"
              ,"marysville"
              ,"pataskala"
              ,"pickaway"
              ,"pickerington"
              ,"plain-city"
              ,"southwest"
              #,"uapl"
              ,"wagnalls")
			  
			  
$polarisVersion = "6.3"

$newStyleCoreDir = "C:\Program Files\Polaris\$polarisVersion\PowerPAC\themes\shared\scss\"
$themeDirRoot = "C:\Program Files\Polaris\$polarisVersion\PowerPAC\custom\themes\"

$removeOldBackup = $false
$copyStyleCore = $true
$rebuildThemes = $true

$updateScssCompile = $false

function Main {
    if ($copyStyleCore) { CopyStyleCoreToThemes }
    if ($updateScssCompile) { UpdateScssCompileScript }
    if ($rebuildThemes) { RebuildThemes }   
}

function CopyStyleCoreToThemes{
    foreach($theme in $themeDirs) {
        $scssDir = "$themeDirRoot\$theme\scss"

        if (test-path "$scssDir\style-core.bak") {
            if ($removeOldBackup) { Remove-Item -Force "$scssDir\style-core.bak"  }
            Write-Host "$theme - style-core backup already exists"
            continue
        }    

        Rename-Item -Path "$scssDir\style-core" -NewName 'style-core.bak'
        Copy-Item -Path $newStyleCoreDir "$scssDir\style-core\" -Force -Recurse
    }
}

function RebuildThemes {
    foreach($theme in $themeDirs){
        Write-Host "Build $theme"
        & "$themeDirRoot\$theme\scss_compile\clc_scss_compile.ps1" -Compact
        Write-Host "---------------------------------"
    }
}

function UpdateScssCompileScript {
    foreach($theme in $themeDirs){
       $scriptPath = "$(Split-path $themeDirRoot)\shared\scss_compile\"
       Copy-Item -Path $scriptPath -Destination "$themeDirRoot\$theme\" -Force -Recurse
    }
}

Main