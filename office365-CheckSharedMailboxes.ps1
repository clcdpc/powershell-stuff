<# Author: Paul Robson

Description: Cmdlets to login to Office 365, and then query Mailboxes and the Shared mailbox type. The ExternalDirectoryObjectId is stored in
a variable.
using that variable, then the users can be queried using the object ID. They are stored in another variable ($shareddisplayname) and displayed

#>

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -Authentication Basic -ConnectionUri https://ps.outlook.com/powershell -AllowRedirection:$true -Credential $UserCredential
Import-PSSession $Session

Connect-MsolService -Credential $UserCredential

$shareddisplayname = @()

$sharedmailboxoid = Get-Mailbox | where {$_.RecipientTypeDetails -eq "SharedMailbox" } | Select -ExpandProperty ExternalDirectoryObjectId

foreach ($oid in $sharedmailboxoid) {

    $shareddisplayname += Get-MsolUser -All | where { $_.ObjectId -eq $oid } | Select-Object Displayname | Sort
	
	Write-Host $shareddisplaynames

}