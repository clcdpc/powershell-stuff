## Look for pattern(s) in files filtering by extension
$htmfiles = @()
$htmfiles = Get-ChildItem -Path "C:\web\polarishelp" -Filter *.htm -Recurse | Select -ExpandProperty FullName

## First pattern to search
$pattern = @()
$pattern = @"
<span class="MCBreadcrumbsPrefix">You are here: </span>
"@

## Second pattern to search
$pattern2 = @"
<div class="content">
"@

## Content to replace first pattern
$newcontent = @"

<table style="width:100%">
<tr><td>
"@

## Content to replace second pattern
$newcontent2 = @"
            </td>
			<td style="text-align:right">
				<form method="GET" action="/search.asp">
					<input type="text" name="zoom_query" size="50">
					<input type="submit" value="Search"> | <a href="/">Back Home</a>
				</form>
			</td></tr>
			</table>
        </div>
"@

$regcontent = $null
$regcontent2 = $null

foreach ($files in $htmfiles) {

    $files2 = $files
    $searchline = (Get-Content $files) | select-string $pattern
    $searchline2 = (Get-Content $files2) | select-string $pattern2
    $regcontent = Get-Content $files
    $regcontent2 = Get-Content $files2
    $linenum = Select-String -Path $files -SimpleMatch $pattern | Select -ExpandProperty LineNumber
    $linenum2 = Select-String -Path $files2 -SimpleMatch $pattern2 | Select -ExpandProperty LineNumber
    Write-Host $linenum
    Write-Host $linenum2
    if($searchline -ne $null) {
        "Match found - replacing string"
        $linechar = (Select-String -Path $files -AllMatches $pattern).Matches.Index
        $templine = $regcontent[$linenum-1].Insert($linechar,$newcontent)
        $regcontent[$linenum-1] = $templine
        $regcontent | Set-Content $files
    }

    if($searchline2 -ne $null) {
        "Match found 2 - replacing string"
        $regcontent[$linenum2-2] = $newcontent2
        $regcontent | Set-Content $files2
        #(Select-String -Path $files2 -AllMatches $pattern2).Matches.Index
    }
}